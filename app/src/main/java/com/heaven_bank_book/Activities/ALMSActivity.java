package com.heaven_bank_book.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.heaven_bank_book.Models.TransectionModel;
import com.heaven_bank_book.R;
import com.heaven_bank_book.Util.ConnectivityReceiver;
import com.heaven_bank_book.Util.Parameters;
import com.heaven_bank_book.Util.SavePref;
import com.heaven_bank_book.Util.util;
import com.heaven_bank_book.parser.AllAPIS;
import com.heaven_bank_book.parser.GetAsync;
import com.heaven_bank_book.parser.GetAsyncPut;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class ALMSActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    ALMSActivity context;
    private SavePref savePref;
    @BindView(R.id.beneficiary_name)
    EditText beneficiaryName;
    @BindView(R.id.currency_5)
    TextView currency5;
    @BindView(R.id.amount)
    EditText amount;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.submit)
    Button submit;
    TransectionModel transectionModel = null;
    boolean is_from_history = false;

    @BindView(R.id.date)
    TextView date_;

    //dd-mm-yyy
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alms);
        ButterKnife.bind(this);
        setToolbar();


        context = ALMSActivity.this;
        savePref = new SavePref(context);

        currency5.setText(savePref.getCurrency());

        is_from_history = getIntent().getBooleanExtra("is_from_history", false);


        date_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        context,
                        now.get(Calendar.YEAR), // Initial year selection
                        now.get(Calendar.MONTH), // Initial month selection
                        now.get(Calendar.DAY_OF_MONTH) // Inital day selection
                );
// If you're calling this from a support Fragment
                dpd.show(getSupportFragmentManager(), "Datepickerdialog");
            }
        });

        if (is_from_history) {
            transectionModel = getIntent().getExtras().getParcelable("model");

            beneficiaryName.setText(transectionModel.getChurch_name());
            amount.setText(util.getDecimalFormattedString(transectionModel.getAmount()));
            description.setText(transectionModel.getDescription());
           date_.setText(util.convertTimeStampDateTime(Long.parseLong(transectionModel.getDate())));

            submit.setVisibility(View.VISIBLE);
            submit.setText("Update");

           /* description.setEnabled(false);
            churchName.setEnabled(false);
            turnover.setEnabled(false);
            costOfSales.setEnabled(false);
            grossIncome.setEnabled(false);
            tithe.setEnabled(false);*/

        }

        amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence st, int start, int before, int count) {
                //if (!st.toString().isEmpty())
                // calculation(true);

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    amount.removeTextChangedListener(this);
                    String value = amount.getText().toString();


                    if (value != null && !value.equals("")) {

                        if (value.startsWith(".")) {
                            amount.setText("0.");
                        }
                        if (value.startsWith("0") && !value.startsWith("0.")) {
                            amount.setText("");

                        }


                        String str = amount.getText().toString().replaceAll(",", "");
                        if (!value.equals(""))
                            amount.setText(util.getDecimalFormattedString(str));
                        amount.setSelection(amount.getText().toString().length());
                    }
                    amount.addTextChangedListener(this);
                    return;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    amount.addTextChangedListener(this);
                }


            }
        });

    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.alms));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }
    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = "You picked the following date: " + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
        // endDateTime.setText(dateTime);

        String date_text = String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear+1) + "-" + String.valueOf(year);

        date_.setText(date_text);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.submit)
    public void onClick() {
        if (ConnectivityReceiver.isConnected()) {
            if (beneficiaryName.getText().toString().isEmpty()) {
                util.IOSDialog(context, "Please Enter Beneficiary Name");
            } else if (amount.getText().toString().isEmpty()) {
                util.IOSDialog(context, "Please Enter Amount");
            } else if (date_.getText().toString().isEmpty()) {
                util.IOSDialog(context, "Please Select Date");
            } else if (description.getText().toString().isEmpty()) {
                util.IOSDialog(context, "Please Enter Description");
            } else {
                Alert();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    public void Alert() {
        new IOSDialog.Builder(context)
                .setMessage("Are you sure to Submit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!is_from_history)
                            ADD_ALMS_API();
                        else
                            Edit_ALMS_API();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }


    private void ADD_ALMS_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.NAME, beneficiaryName.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.AMOUNT, amount.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, description.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.DATE, util.date_to_timestamp(date_.getText().toString().replace(",", "").trim()));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.ADD_ALMS, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            finish();
                            util.showToast(context, "ALMS Added Sucessfully!!!");

                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.IOSDialog(context, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void Edit_ALMS_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (is_from_history)
            formBuilder.addFormDataPart(Parameters.ALMS_ID, transectionModel.getId());
        formBuilder.addFormDataPart(Parameters.DATE, util.date_to_timestamp(date_.getText().toString().replace(",", "").trim()));
        formBuilder.addFormDataPart(Parameters.NAME, beneficiaryName.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.AMOUNT, amount.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, description.getText().toString().replace(",", "").trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.ADD_ALMS, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            util.showToast(context, "ALMS Updated Sucessfully!!!");
                            Intent intent = new Intent();
                            intent.putExtra("type", getIntent().getStringExtra("type"));
                            setResult(RESULT_OK, intent);
                            finish();


                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.IOSDialog(context, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

}
