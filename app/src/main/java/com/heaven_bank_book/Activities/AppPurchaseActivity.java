package com.heaven_bank_book.Activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.aemerse.iap.DataWrappers;
import com.aemerse.iap.IapConnector;
import com.aemerse.iap.SubscriptionServiceListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.heaven_bank_book.MainActivity;
import com.heaven_bank_book.R;
import com.heaven_bank_book.Util.ConnectivityReceiver;
import com.heaven_bank_book.Util.SavePref;
import com.heaven_bank_book.Util.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AppPurchaseActivity extends AppCompatActivity {

    private static final String TAG = AppPurchaseActivity.class.getName();

    @BindView(R.id.purchase)
    Button purchase;
    @BindView(R.id.no_thanks)
    TextView noThanks;
    @BindView(R.id.price)
    TextView price_textview;
    Context context;

    private SavePref savePref;
    private String auth_key;

    AppPurchaseActivity activity;

    private IapConnector iapConnector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_app_purchse);
        ButterKnife.bind(this);

        activity = AppPurchaseActivity.this;
        savePref = new SavePref(activity);
        auth_key = getIntent().getStringExtra("auth_key");

        setUpBilling();

    }

    @OnClick({R.id.purchase, R.id.no_thanks})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.purchase:
                ProceedPayment();
                break;
            case R.id.no_thanks:
                this.finishAffinity();
                break;
        }
    }

    private void setUpBilling() {
        savePref.setBoolean(util.SET_SUBSCRIBED, false);

        ArrayList<String> consumableKeys = new ArrayList<>();
        ArrayList<String> subscriptionKeys = new ArrayList<>();
        subscriptionKeys.add(util.MONTHLY_SUBSCRIPTION);

        iapConnector = new IapConnector(context = this
                , consumableKeys, // pass the list of non-consumables
                consumableKeys, // pass the list of consumables
                subscriptionKeys, // pass the list of subscriptions
                util.LICENSE_KEY, true // pass your app's license keytrue // to enable / disable logging
        );

        iapConnector.addSubscriptionListener(new SubscriptionServiceListener() {
            @Override
            public void onSubscriptionRestored(@NonNull DataWrappers.PurchaseInfo purchaseInfo) {

            }

            @Override
            public void onSubscriptionPurchased(@NonNull DataWrappers.PurchaseInfo purchaseInfo) {
                Log.e("Subscribe", "yes" + purchaseInfo.getSku());
                if (purchaseInfo.getSku().equals(util.MONTHLY_SUBSCRIPTION))
                    savePref.setBoolean(util.SET_SUBSCRIBED, true);
                finish();
            }

            @Override
            public void onPricesUpdated(@NonNull Map<String, DataWrappers.SkuDetails> map) {
                try {

                    if (!map.isEmpty()) {
                        DataWrappers.SkuDetails details = map.get("pro_monthly");
                        if (details != null) {
                            String price = details.getPrice();
                            String currency = details.getPriceCurrencyCode();
                            savePref.setString("subscription_price", price + " " + currency);
                            //price_textview.setText(currency + " " + price);
                        }

                    }


                } catch (Exception e) {
                    Log.e("Exception ", "is " + e.getMessage());
                }
            }
        });

    }


    public void ProceedPayment() {
        if (ConnectivityReceiver.isConnected()) {
            iapConnector.subscribe(this, util.MONTHLY_SUBSCRIPTION);
            //bp.subs
            //            CardEntry.startCardEntryActivity(context, true,
//                    DEFAULT_CARD_ENTRY_REQUEST_CODE);
        } else {
            util.IOSDialog(this, util.internet_Connection_Error);
        }

    }


}
