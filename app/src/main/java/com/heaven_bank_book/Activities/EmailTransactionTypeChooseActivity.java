package com.heaven_bank_book.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.heaven_bank_book.R;
import com.heaven_bank_book.Util.ConnectivityReceiver;
import com.heaven_bank_book.Util.Parameters;
import com.heaven_bank_book.Util.SavePref;
import com.heaven_bank_book.Util.util;
import com.heaven_bank_book.parser.AllAPIS;
import com.heaven_bank_book.parser.GetAsync;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class EmailTransactionTypeChooseActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    EmailTransactionTypeChooseActivity context;
    @BindView(R.id.from_date)
    TextView fromDate;
    @BindView(R.id.to_date)
    TextView toDate;
    @BindView(R.id.tithe_rb)
    RadioButton titheRb;
    @BindView(R.id.seeds_rb)
    RadioButton seedsRb;
    @BindView(R.id.alms_rb)
    RadioButton almsRb;
    @BindView(R.id.email_transaction_history)
    Button emailTransactionHistory;
    @BindView(R.id.transactions_rb)
    RadioButton transactionsRb;
    private SavePref savePref;
    String type="";
    String from_date_text = "";
    String to_date_text = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_transaction_type_choose);
        ButterKnife.bind(this);
        setToolbar();

        context = EmailTransactionTypeChooseActivity.this;
        savePref = new SavePref(context);

    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Email Transaction");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void Alert() {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SEND_MAIL_API();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void SEND_MAIL_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.FROM_DATE, from_date_text);
        formBuilder.add(Parameters.TO_DATE, to_date_text);
        if (titheRb.isChecked())
            formBuilder.add(Parameters.TYPE, "2");//1 seed 2-> tithe 3 -> widthdral 4>alms
        else if (seedsRb.isChecked())
            formBuilder.add(Parameters.TYPE, "1");//1 seed 2-> tithe 3 -> widthdral 4>alms
        else if (almsRb.isChecked())
            formBuilder.add(Parameters.TYPE, "4");//1 seed 2-> tithe 3 -> widthdral 4>alms
        else if (transactionsRb.isChecked())
            formBuilder.add(Parameters.TYPE, "3");//1 seed 2-> tithe 3 -> widthdral 4>alms
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.SEND_MAIL, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.IOSDialog(context, "Transaction History Sent Sucessfully!");
                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.IOSDialog(context, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void open_calender() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                context,
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
// If you're calling this from a support Fragment
        dpd.show(getSupportFragmentManager(), "Datepickerdialog");

    }

    @OnClick({R.id.from_date, R.id.to_date, R.id.email_transaction_history})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.from_date:
                type = "1";
                /*new SingleDateAndTimePickerDialog.Builder(context)
                        .bottomSheet()
                        .curved()
                        // .minutesStep(15)
                        .displayHours(false)
                        .displayMinutes(false)
                        //.todayText("aujourd'hui")
                        .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                            @Override
                            public void onDisplayed(SingleDateAndTimePicker picker) {
                                //retrieve the SingleDateAndTimePicker
                            }
                        })

                        .title("Select Date")
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date) {
                               *//* Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);

                                endDateTime.setText(dateTime);*//*


                                Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                SimpleDateFormat dateFormat_new = new SimpleDateFormat("MM/dd/yyyy");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);


                                if (!toDate.getText().toString().isEmpty()) {
                                    boolean b = CheckDates(dateTime, toDate.getText().toString());

                                    if (b) {
                                        from_date_text = dateFormat_new.format(date);
                                        fromDate.setText(dateTime);
                                        //hit api
                                    } else {
                                        // fromDateTithe.setText("");
                                        util.IOSDialog(context, "From date should be lesser then to date.");
                                    }
                                } else {
                                    from_date_text = dateFormat_new.format(date);
                                    fromDate.setText(dateTime);

                                }


                            }
                        }).display();*/
                open_calender();
                break;
            case R.id.to_date:
                type = "2";
                /*new SingleDateAndTimePickerDialog.Builder(context)
                        .bottomSheet()
                        .curved()
                        // .minutesStep(15)
                        .displayHours(false)
                        .displayMinutes(false)
                        //.todayText("aujourd'hui")
                        .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                            @Override
                            public void onDisplayed(SingleDateAndTimePicker picker) {
                                //retrieve the SingleDateAndTimePicker
                            }
                        })

                        .title("Select Date")
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date) {
                               *//* Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);

                                endDateTime.setText(dateTime);*//*


                                Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                SimpleDateFormat dateFormat_new = new SimpleDateFormat("MM/dd/yyyy");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);

                                if (!fromDate.getText().toString().isEmpty()) {
                                    boolean b = CheckDates(fromDate.getText().toString(), dateTime);

                                    if (b) {
                                        to_date_text = dateFormat_new.format(date);
                                        toDate.setText(dateTime);
                                        //hit api
                                    } else {
                                        to_date_text = "";
                                        toDate.setText("");
                                        util.IOSDialog(context, "To date should be greater then from date.");
                                    }
                                } else {
                                    util.IOSDialog(context, "Please select from date first");
                                }

                            }
                        }).display();*/

                open_calender();

                break;
            case R.id.email_transaction_history:
                if (ConnectivityReceiver.isConnected()) {
                    if (fromDate.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please select from Date");
                    } else if (fromDate.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please select to Date");
                    } else {
                        Alert();
                    }
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }

                break;
        }
    }

    public static boolean CheckDates(String d1, String d2) {
        SimpleDateFormat dfDate = new SimpleDateFormat("dd/MM/yyyy");
        boolean b = false;
        try {
            if (dfDate.parse(d1).before(dfDate.parse(d2))) {
                b = true;//If start date is before end date
            } else if (dfDate.parse(d1).equals(dfDate.parse(d2))) {
                b = false;//If two dates are equal
            } else {
                b = false; //If start date is after the end date
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return b;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {


        String dateTime = String.valueOf(dayOfMonth) + "/" + String.valueOf(monthOfYear+1) + "/" + String.valueOf(year);
        String date_text_new = String.valueOf(monthOfYear+1) + "/" + String.valueOf(dayOfMonth) + "/" + String.valueOf(year);

        if (type.equals("1")) {


            if (!toDate.getText().toString().isEmpty()) {
                boolean b = CheckDates(dateTime, toDate.getText().toString());

                if (b) {
                    from_date_text = date_text_new;
                    fromDate.setText(dateTime);
                    //hit api
                } else {
                    // fromDateTithe.setText("");
                    util.IOSDialog(context, "From date should be lesser then to date.");
                }
            } else {
                from_date_text = date_text_new;
                fromDate.setText(dateTime);

            }
        } else {
            if (!fromDate.getText().toString().isEmpty()) {
                boolean b = CheckDates(fromDate.getText().toString(), dateTime);

                if (b) {
                    to_date_text = date_text_new;
                    toDate.setText(dateTime);
                    //hit api
                } else {
                    to_date_text = "";
                    toDate.setText("");
                    util.IOSDialog(context, "To date should be greater then from date.");
                }
            } else {
                util.IOSDialog(context, "Please select from date first");
            }
        }


    }
}
