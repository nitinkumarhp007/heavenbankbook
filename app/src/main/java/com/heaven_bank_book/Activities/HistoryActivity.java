package com.heaven_bank_book.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.heaven_bank_book.Adapters.HistoryAdapter;
import com.heaven_bank_book.Models.TransectionModel;
import com.heaven_bank_book.R;
import com.heaven_bank_book.Util.ConnectivityReceiver;
import com.heaven_bank_book.Util.Parameters;
import com.heaven_bank_book.Util.SavePref;
import com.heaven_bank_book.Util.util;
import com.heaven_bank_book.parser.AllAPIS;
import com.heaven_bank_book.parser.GetAsyncDELETE;
import com.heaven_bank_book.parser.GetAsyncGet;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HistoryActivity extends AppCompatActivity {
    HistoryActivity context;
    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    @BindView(R.id.email_transaction_history)
    Button emailTransactionHistory;
    @BindView(R.id.error_text)
    TextView errorText;
    @BindView(R.id.tithe)
    Button tithe;
    @BindView(R.id.seeds)
    Button seeds;
    @BindView(R.id.alms)
    Button alms;
    @BindView(R.id.withdrawal)
    Button withdrawal;
    @BindView(R.id.v1)
    View v1;
    @BindView(R.id.v2)
    View v2;
    @BindView(R.id.v3)
    View v3;
    @BindView(R.id.v4)
    View v4;

    private SavePref savePref;

    String selected_item = "1";

    private ArrayList<TransectionModel> list_tithe;
    private ArrayList<TransectionModel> list_alms;
    private ArrayList<TransectionModel> list_seeds;
    private ArrayList<TransectionModel> list_withdrawal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);
        setToolbar();
        context = HistoryActivity.this;
        savePref = new SavePref(context);

        if (ConnectivityReceiver.isConnected())
            TRANSACTIONS();
        else
            util.IOSDialog(context, util.internet_Connection_Error);
    }


    private void TRANSACTIONS() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.TRANSACTIONS, formBody) {
            @Override
            public void getValueParse(String result) {
                list_seeds = new ArrayList<>();
                list_tithe = new ArrayList<>();
                list_alms = new ArrayList<>();
                list_withdrawal = new ArrayList<>();
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                            JSONArray data = jsonmainObject.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject object = data.getJSONObject(i);

                                TransectionModel transectionModel = new TransectionModel();
                                transectionModel.setType(object.getString("type"));
                                transectionModel.setAmount(object.getString("amount"));
                                transectionModel.setCreated(object.getString("created"));
                                transectionModel.setDescription(object.getString("description"));
                                transectionModel.setId(object.getString("type_id"));
                                transectionModel.setTotal(object.getString("total"));
                                transectionModel.setChurch_name(object.getString("church_name"));
                                transectionModel.setPetitions(object.getString("petitions"));
                                transectionModel.setDate(object.getString("date"));
                                if (object.getString("type").equals("1") || object.getString("type").equals("2")) {
                                    transectionModel.setReturn_expection(object.getJSONObject("full_details").getString("return_expection"));
                                    transectionModel.setEarth_measure(object.getJSONObject("full_details").getString("earth_measure"));
                                    transectionModel.setHeaven_measure(object.getJSONObject("full_details").getString("heaven_measure"));
                                    transectionModel.setBalance_based_per(object.getJSONObject("full_details").getString("balance_based_per"));
                                    transectionModel.setTurn_over(object.getJSONObject("full_details").getString("turn_over"));
                                    transectionModel.setSale_cost(object.getJSONObject("full_details").getString("sale_cost"));
                                    transectionModel.setGross_income(object.getJSONObject("full_details").getString("gross_income"));
                                    transectionModel.setBalance(object.getJSONObject("full_details").getString("balance"));

                                }


                                if (object.getString("type").equals("2")) {
                                    list_tithe.add(transectionModel);
                                } else if (object.getString("type").equals("1")) {
                                    list_seeds.add(transectionModel);
                                } else if (object.getString("type").equals("3")) {
                                    list_withdrawal.add(transectionModel);
                                } else if (object.getString("type").equals("4")) {
                                    list_alms.add(transectionModel);
                                }
                            }

                            if (selected_item.equals("1")) {
                                if (list_tithe.size() > 0) {
                                    selected_item = "1";
                                    myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                    myRecyclerView.setAdapter(new HistoryAdapter(context, list_tithe));
                                    myRecyclerView.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.GONE);
                                } else {
                                    errorText.setVisibility(View.VISIBLE);
                                    myRecyclerView.setVisibility(View.GONE);
                                }
                            } else if (selected_item.equals("2")) {
                                if (list_seeds.size() > 0) {
                                    selected_item = "2";
                                    myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                    myRecyclerView.setAdapter(new HistoryAdapter(context, list_seeds));
                                    myRecyclerView.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.GONE);
                                } else {
                                    errorText.setVisibility(View.VISIBLE);
                                    myRecyclerView.setVisibility(View.GONE);
                                }
                            } else {
                                if (list_seeds.size() > 0) {
                                    selected_item = "4";
                                    myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                                    myRecyclerView.setAdapter(new HistoryAdapter(context, list_alms));
                                    myRecyclerView.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.GONE);
                                } else {
                                    errorText.setVisibility(View.VISIBLE);
                                    myRecyclerView.setVisibility(View.GONE);
                                }
                            }

                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public void next_task(String type, int position) {
        Intent intent = null;
        if (type.equals("1")) {
            // Seeds
            intent = new Intent(context, SeedsActivity.class);
            intent.putExtra("model", list_seeds.get(position));
        } else if (type.equals("2")) {
            // Tithe
            intent = new Intent(context, TitheActivity.class);
            intent.putExtra("model", list_tithe.get(position));
        } else if (type.equals("4")) {
            // Tithe
            intent = new Intent(context, ALMSActivity.class);
            intent.putExtra("model", list_alms.get(position));
        }
        intent.putExtra("is_from_history", true);
        intent.putExtra("type", type);
        context.startActivityForResult(intent, 333);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 333) {
                if (data.getStringExtra("type").equals("2")) {
                    selected_item = "1";
                    All_unselcted();
                    tithe.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    v1.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                } else if (data.getStringExtra("type").equals("1")) {
                    selected_item = "2";
                    All_unselcted();
                    seeds.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    v2.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                } else {
                    selected_item = "3";
                    All_unselcted();
                    alms.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                    v3.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                }
                if (ConnectivityReceiver.isConnected()) {
                    TRANSACTIONS();
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
            }
        }
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("History");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick(R.id.email_transaction_history)
    public void onViewClicked() {
        startActivity(new Intent(this, EmailTransactionTypeChooseActivity.class));
        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);

    }


    @OnClick({R.id.tithe, R.id.seeds, R.id.alms, R.id.withdrawal})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tithe:
                selected_item = "1";
                All_unselcted();
                tithe.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                v1.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                if (list_tithe.size() > 0) {
                    myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                    myRecyclerView.setAdapter(new HistoryAdapter(context, list_tithe));
                    myRecyclerView.setVisibility(View.VISIBLE);
                    errorText.setVisibility(View.GONE);
                } else {
                    errorText.setVisibility(View.VISIBLE);
                    myRecyclerView.setVisibility(View.GONE);
                }
                break;
            case R.id.seeds:
                selected_item = "2";
                All_unselcted();
                seeds.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                v2.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                if (list_seeds.size() > 0) {
                    myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                    myRecyclerView.setAdapter(new HistoryAdapter(context, list_seeds));
                    myRecyclerView.setVisibility(View.VISIBLE);
                    errorText.setVisibility(View.GONE);
                } else {
                    errorText.setVisibility(View.VISIBLE);
                    myRecyclerView.setVisibility(View.GONE);
                }
                break;
            case R.id.alms:
                selected_item = "4";
                All_unselcted();
                alms.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                v3.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                if (list_alms.size() > 0) {
                    myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                    myRecyclerView.setAdapter(new HistoryAdapter(context, list_alms));
                    myRecyclerView.setVisibility(View.VISIBLE);
                    errorText.setVisibility(View.GONE);
                } else {
                    errorText.setVisibility(View.VISIBLE);
                    myRecyclerView.setVisibility(View.GONE);
                }
                break;
            case R.id.withdrawal:
                selected_item = "3";
                All_unselcted();
                withdrawal.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                v4.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                if (list_withdrawal.size() > 0) {
                    myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                    myRecyclerView.setAdapter(new HistoryAdapter(context, list_withdrawal));
                    myRecyclerView.setVisibility(View.VISIBLE);
                    errorText.setVisibility(View.GONE);
                } else {
                    errorText.setVisibility(View.VISIBLE);
                    myRecyclerView.setVisibility(View.GONE);
                }
                break;
        }
    }

    private void All_unselcted() {
        tithe.setTextColor(getResources().getColor(R.color.black));
        seeds.setTextColor(getResources().getColor(R.color.black));
        withdrawal.setTextColor(getResources().getColor(R.color.black));
        alms.setTextColor(getResources().getColor(R.color.black));
        v1.setBackgroundColor(getResources().getColor(R.color.white));
        v2.setBackgroundColor(getResources().getColor(R.color.white));
        v3.setBackgroundColor(getResources().getColor(R.color.white));
        v4.setBackgroundColor(getResources().getColor(R.color.white));
    }

    public void DELETEAlert(final int position) {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Are you sure to Delete?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String id = "";
                        if (selected_item.equals("1")) {
                            id = list_tithe.get(position).getId();
                        } else if (selected_item.equals("2")) {
                            id = list_seeds.get(position).getId();
                        } else if (selected_item.equals("3")) {
                            id = list_withdrawal.get(position).getId();
                        }
                        DELETE_ADDRESS(id, position);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    public void DELETE_ADDRESS(String id, final int position) {
        final ProgressDialog dialog = util.initializeProgress(context);
        dialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.TRANSACTION_ID, id);
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncDELETE mAsync = new GetAsyncDELETE(context, AllAPIS.TRANSACTIONS, formBody, dialog, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(Response result) {
                //list = new ArrayList<>();
                dialog.dismiss();
                String r = result.body().toString();
                if (result.isSuccessful()) {
                    Toast.makeText(context, "Transaction Deleted Successfully!", Toast.LENGTH_SHORT).show();
                    String id = "";
                    myRecyclerView.setLayoutManager(new LinearLayoutManager(context));
                    if (selected_item.equals("1")) {
                        list_tithe.remove(position);
                        if (list_tithe.size() > 0) {
                            myRecyclerView.setAdapter(new HistoryAdapter(context, list_tithe));
                            myRecyclerView.setVisibility(View.VISIBLE);
                            errorText.setVisibility(View.GONE);
                        } else {
                            errorText.setVisibility(View.VISIBLE);
                            myRecyclerView.setVisibility(View.GONE);
                        }
                    } else if (selected_item.equals("2")) {
                        list_seeds.remove(position);
                        if (list_seeds.size() > 0) {
                            myRecyclerView.setAdapter(new HistoryAdapter(context, list_seeds));
                            myRecyclerView.setVisibility(View.VISIBLE);
                            errorText.setVisibility(View.GONE);
                        } else {
                            errorText.setVisibility(View.VISIBLE);
                            myRecyclerView.setVisibility(View.GONE);
                        }
                    } else if (selected_item.equals("3")) {
                        list_withdrawal.remove(position);
                        if (list_withdrawal.size() > 0) {
                            myRecyclerView.setAdapter(new HistoryAdapter(context, list_withdrawal));
                            myRecyclerView.setVisibility(View.VISIBLE);
                            errorText.setVisibility(View.GONE);
                        } else {
                            errorText.setVisibility(View.VISIBLE);
                            myRecyclerView.setVisibility(View.GONE);
                        }
                    }
                   /* try {
                        JSONObject jsonmainObject = new JSONObject(result.toString());
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            Toast.makeText(context, "Alert Deleted Successfully!", Toast.LENGTH_SHORT).show();
                            list.remove(position);
                            adapter.notifyDataSetChanged();
                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }*/
                }
            }


            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }
}
