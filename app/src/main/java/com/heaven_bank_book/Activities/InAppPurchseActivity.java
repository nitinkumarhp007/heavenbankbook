package com.heaven_bank_book.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.ConsumeResponseListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.heaven_bank_book.MainActivity;
import com.heaven_bank_book.R;
import com.heaven_bank_book.Util.Parameters;
import com.heaven_bank_book.Util.SavePref;
import com.heaven_bank_book.Util.util;
import com.heaven_bank_book.parser.AllAPIS;
import com.heaven_bank_book.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class InAppPurchseActivity extends AppCompatActivity {

    InAppPurchseActivity context;
    @BindView(R.id.purchase)
    Button purchase;
    @BindView(R.id.no_thanks)
    TextView noThanks;

    BillingClient mBillingClient;

    String subscription_week = "monthly_1_20";
    private SavePref savePref;

    String auth_key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_in_app_purchse);
        ButterKnife.bind(this);

        context = InAppPurchseActivity.this;
        savePref = new SavePref(context);
        auth_key = getIntent().getStringExtra("auth_key");


    }

    @OnClick({R.id.purchase, R.id.no_thanks})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.purchase:
                //setSubscription_week();
                break;
            case R.id.no_thanks:
                this.finishAffinity();
                break;
        }
    }

    private void IN_APP_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        int time = (int) (System.currentTimeMillis());
        Timestamp tsTemp = new Timestamp(time);
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.GMAIL_ACCOUNT, getIntent().getStringExtra("gmail"));
        formBuilder.add(Parameters.DATE, tsTemp.toString());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.IN_APP, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            savePref.setAuthorization_key(auth_key);
                            Intent intent = new Intent(context, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            util.showToast(context, "Login Sucessfully!!!");
                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {

                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.IOSDialog(context, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


}
