package com.heaven_bank_book.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;

import com.bumptech.glide.Glide;
import com.heaven_bank_book.R;
import com.heaven_bank_book.Util.SavePref;
import com.heaven_bank_book.Util.util;
import com.heaven_bank_book.parser.AllAPIS;
import com.heaven_bank_book.parser.GetAsync;
import com.ligl.android.widget.iosdialog.IOSDialog;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.FormBody;
import okhttp3.RequestBody;

public class ProfileActivity extends AppCompatActivity {

    @BindView(R.id.profile_pic)
    CircleImageView profilePic;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.edit_profile_text)
    TextView editProfileText;
    @BindView(R.id.edit_profile)
    RelativeLayout editProfile;
    @BindView(R.id.change_password_text)
    TextView changePasswordText;
    @BindView(R.id.change_password)
    RelativeLayout changePassword;
    @BindView(R.id.privacy_policy_text)
    TextView privacyPolicyText;
    @BindView(R.id.privacy_policy)
    RelativeLayout privacyPolicy;
    @BindView(R.id.about_us_text)
    TextView aboutUsText;
    @BindView(R.id.about_us)
    RelativeLayout aboutUs;
    @BindView(R.id.logout_text)
    TextView logoutText;
    @BindView(R.id.logout)
    CardView logout;

    ProfileActivity context;
    private SavePref savePref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        setToolbar();
        context = ProfileActivity.this;
        savePref = new SavePref(context);
    }

    @Override
    protected void onResume() {
        super.onResume();
        setdata();
    }

    private void setdata() {
        name.setText(savePref.getName());
        email.setText(savePref.getEmail());
        Glide.with(context).load(savePref.getImage()).error(R.drawable.place_holder).into(profilePic);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Profile");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick({R.id.edit_profile_text, R.id.edit_profile, R.id.change_password_text, R.id.change_password, R.id.privacy_policy_text, R.id.privacy_policy, R.id.about_us_text, R.id.about_us, R.id.logout_text, R.id.logout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.edit_profile_text:
                if (savePref.getIs_soical()) {
                    util.IOSDialog(context, "Not Allowed to Update Profile when you are social logined");
                } else {
                    startActivity(new Intent(this, UpdateProfileActivity.class));
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                }

                break;
            case R.id.edit_profile:
                if (savePref.getIs_soical()) {
                    util.IOSDialog(context, "Not Allowed to Update Profile when you are social logined");
                } else {
                    startActivity(new Intent(this, UpdateProfileActivity.class));
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                }
                break;
            case R.id.change_password_text:
                if (savePref.getIs_soical()) {
                    util.IOSDialog(context, "Not Allowed to Change Password when you are social logined");
                } else {
                    startActivity(new Intent(this, ChangePasswordActivity.class));
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                }

                break;
            case R.id.change_password:
                if (savePref.getIs_soical()) {
                    util.IOSDialog(context, "Not Allowed to Change Password when you are social logined");
                } else {
                    startActivity(new Intent(this, ChangePasswordActivity.class));
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                }
                break;
            case R.id.privacy_policy_text:
                Intent intent = new Intent(context, TermConditionActivity.class);
                intent.putExtra("type", "term");
                startActivity(intent);
                break;
            case R.id.privacy_policy:
                Intent intent1 = new Intent(context, TermConditionActivity.class);
                intent1.putExtra("type", "term");
                startActivity(intent1);
                break;
            case R.id.about_us_text:
                Intent intent2 = new Intent(context, TermConditionActivity.class);
                intent2.putExtra("type", "about");
                startActivity(intent2);
                break;
            case R.id.about_us:
                Intent intent4 = new Intent(context, TermConditionActivity.class);
                intent4.putExtra("type", "about");
                startActivity(intent4);
                break;
            case R.id.logout_text:
                LogoutAlert();
                break;
            case R.id.logout:
                LogoutAlert();
                break;
        }
    }

    private void LOGOUT_API() {
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.LOGOUT, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                             savePref.setAuthorization_key("");
                            util.showToast(context, "User Logout Successfully!");
                            Intent intent = new Intent(context, SignInActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.IOSDialog(context, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void LogoutAlert() {
        new IOSDialog.Builder(context)
                .setTitle(context.getResources().getString(R.string.app_name))
                .setMessage("Are you sure to Logout from the App?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        LOGOUT_API();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }
}
