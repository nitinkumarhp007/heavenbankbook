package com.heaven_bank_book.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.heaven_bank_book.R;
import com.heaven_bank_book.Util.Parameters;
import com.heaven_bank_book.Util.SavePref;
import com.heaven_bank_book.Util.util;
import com.heaven_bank_book.parser.AllAPIS;
import com.heaven_bank_book.parser.GetAsyncGet;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class QuickViewActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    QuickViewActivity context;
    @BindView(R.id.from_date_tithe)
    TextView fromDateTithe;
    @BindView(R.id.to_date_tithe)
    TextView toDateTithe;
    @BindView(R.id.tithe_amount)
    TextView titheAmount;
    @BindView(R.id.from_date_seeds)
    TextView fromDateSeeds;
    @BindView(R.id.to_date_seeds)
    TextView toDateSeeds;
    @BindView(R.id.seeds_amount)
    TextView seedsAmount;
    @BindView(R.id.from_date_withdrawals)
    TextView fromDateWithdrawals;
    @BindView(R.id.to_date_withdrawals)
    TextView toDateWithdrawals;
    @BindView(R.id.withdrawal_amount)
    TextView withdrawalAmount;
    @BindView(R.id.from_date_alms)
    TextView fromDateAlms;
    @BindView(R.id.to_date_alms)
    TextView toDateAlms;
    @BindView(R.id.alms_amount)
    TextView almsAmount;
    private SavePref savePref;

    String f_d_tithe = "";
    String t_d_tithe = "";
    String f_d_alms = "";
    String t_d_alms = "";
    String f_d_seeds = "";
    String t_d_seeds = "";
    String f_d_withdrawal = "";
    String t_d_withdrawal = "";

    String selected = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quick_view);
        ButterKnife.bind(this);

        context = QuickViewActivity.this;
        savePref = new SavePref(context);
        setToolbar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Quick View");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = "You picked the following date: " + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
        // endDateTime.setText(dateTime);

        String dateTime = String.valueOf(dayOfMonth) + "/" + String.valueOf(monthOfYear + 1) + "/" + String.valueOf(year);
        String date_text_new = String.valueOf(monthOfYear + 1) + "/" + String.valueOf(dayOfMonth) + "/" + String.valueOf(year);

        if (selected.equals("1")) {
            if (!toDateAlms.getText().toString().isEmpty()) {
                boolean b = CheckDates(dateTime, toDateAlms.getText().toString());

                if (b) {
                    f_d_alms = date_text_new;
                    fromDateAlms.setText(dateTime);
                    GET_BALANCE_FILTER(f_d_alms,
                            t_d_alms, "4");
                } else {
                    // fromDateTithe.setText("");
                    util.IOSDialog(context, "From date should be lesser then to date.");
                }
            } else {
                f_d_alms = date_text_new;
                fromDateAlms.setText(dateTime);

            }
        } else if (selected.equals("2")) {
            if (!fromDateAlms.getText().toString().isEmpty()) {
                boolean b = CheckDates(fromDateAlms.getText().toString(), dateTime);

                if (b) {
                    t_d_alms = date_text_new;
                    toDateAlms.setText(dateTime);
                    GET_BALANCE_FILTER(f_d_alms,
                            t_d_alms, "4");
                } else {
                    t_d_alms = "";
                    toDateAlms.setText("");
                    util.IOSDialog(context, "To date should be greater then from date.");
                }
            } else {
                util.IOSDialog(context, "Please select from date first");
            }
        } else if (selected.equals("3")) {
            if (!toDateTithe.getText().toString().isEmpty()) {
                boolean b = CheckDates(dateTime, toDateTithe.getText().toString());

                if (b) {
                    f_d_tithe = date_text_new;
                    fromDateTithe.setText(dateTime);
                    GET_BALANCE_FILTER(f_d_tithe,
                            t_d_tithe, "2");
                } else {
                    // fromDateTithe.setText("");
                    util.IOSDialog(context, "From date should be lesser then to date.");
                }
            } else {
                f_d_tithe = date_text_new;
                fromDateTithe.setText(dateTime);

            }


        } else if (selected.equals("4")) {
            if (!fromDateTithe.getText().toString().isEmpty()) {
                boolean b = CheckDates(fromDateTithe.getText().toString(), dateTime);

                if (b) {
                    t_d_tithe = date_text_new;
                    toDateTithe.setText(dateTime);
                    GET_BALANCE_FILTER(f_d_tithe,
                            t_d_tithe, "2");
                } else {
                    t_d_tithe = "";
                    toDateTithe.setText("");
                    util.IOSDialog(context, "To date should be greater then from date.");
                }
            } else {
                util.IOSDialog(context, "Please select from date first");
            }
        } else if (selected.equals("5")) {
            if (!toDateSeeds.getText().toString().isEmpty()) {
                boolean b = CheckDates(dateTime, toDateSeeds.getText().toString());

                if (b) {
                    f_d_seeds = date_text_new;
                    fromDateSeeds.setText(dateTime);
                    GET_BALANCE_FILTER(f_d_seeds,
                            t_d_seeds, "1");
                } else {
                    // fromDateTithe.setText("");
                    util.IOSDialog(context, "From date should be lesser then to date.");
                }
            } else {
                f_d_seeds = date_text_new;
                fromDateSeeds.setText(dateTime);

            }
        } else if (selected.equals("6")) {
            if (!fromDateSeeds.getText().toString().isEmpty()) {
                boolean b = CheckDates(fromDateSeeds.getText().toString(), dateTime);

                if (b) {
                    t_d_seeds = date_text_new;
                    toDateSeeds.setText(dateTime);
                    GET_BALANCE_FILTER(f_d_seeds,
                            t_d_seeds, "1");
                } else {
                    toDateSeeds.setText("");
                    util.IOSDialog(context, "To date should be greater then from date.");
                }
            } else {
                util.IOSDialog(context, "Please select from date first");
            }

        } else if (selected.equals("7")) {
            if (!toDateWithdrawals.getText().toString().isEmpty()) {
                boolean b = CheckDates(dateTime, toDateWithdrawals.getText().toString());

                if (b) {
                    f_d_withdrawal = date_text_new;
                    fromDateWithdrawals.setText(dateTime);
                    GET_BALANCE_FILTER(f_d_withdrawal,
                            t_d_withdrawal, "3");
                } else {
                    // fromDateTithe.setText("");
                    util.IOSDialog(context, "From date should be lesser then to date.");
                }
            } else {
                f_d_withdrawal = date_text_new;
                fromDateWithdrawals.setText(dateTime);

            }
        } else if (selected.equals("8")) {
            if (!fromDateWithdrawals.getText().toString().isEmpty()) {
                boolean b = CheckDates(fromDateWithdrawals.getText().toString(), dateTime);

                if (b) {
                    t_d_withdrawal = date_text_new;
                    toDateWithdrawals.setText(dateTime);
                    GET_BALANCE_FILTER(f_d_withdrawal,
                            t_d_withdrawal, "3");
                } else {
                    toDateWithdrawals.setText("");
                    util.IOSDialog(context, "To date should be greater then from date.");
                }
            } else {
                util.IOSDialog(context, "Please select from date first");
            }
        }
    }

    private void open_calender() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                context,
                now.get(Calendar.YEAR), // Initial year selection
                now.get(Calendar.MONTH), // Initial month selection
                now.get(Calendar.DAY_OF_MONTH) // Inital day selection
        );
// If you're calling this from a support Fragment
        dpd.show(getSupportFragmentManager(), "Datepickerdialog");

    }

    @OnClick({R.id.from_date_tithe, R.id.to_date_tithe, R.id.from_date_seeds, R.id.to_date_seeds, R.id.from_date_alms, R.id.to_date_alms, R.id.from_date_withdrawals, R.id.to_date_withdrawals})
    public void onClick(View view) {
        open_calender();
        switch (view.getId()) {
            case R.id.from_date_alms:

                selected = "1";




                /*new SingleDateAndTimePickerDialog.Builder(context)
                        .bottomSheet()
                        .curved()
                        // .minutesStep(15)
                        .displayHours(false)
                        .displayMinutes(false)
                        //.todayText("aujourd'hui")
                        .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                            @Override
                            public void onDisplayed(SingleDateAndTimePicker picker) {
                                //retrieve the SingleDateAndTimePicker
                            }
                        })

                        .title("Select Date")
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date) {
                               *//* Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);

                                endDateTime.setText(dateTime);*//*


                                Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                SimpleDateFormat dateFormat_new = new SimpleDateFormat("MM/dd/yyyy");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);


                                if (!toDateAlms.getText().toString().isEmpty()) {
                                    boolean b = CheckDates(dateTime, toDateAlms.getText().toString());

                                    if (b) {
                                        f_d_alms = dateFormat_new.format(date);
                                        fromDateAlms.setText(dateTime);
                                        GET_BALANCE_FILTER(f_d_alms,
                                                t_d_alms, "4");
                                    } else {
                                        // fromDateTithe.setText("");
                                        util.IOSDialog(context, "From date should be lesser then to date.");
                                    }
                                } else {
                                    f_d_alms = dateFormat_new.format(date);
                                    fromDateAlms.setText(dateTime);

                                }


                            }
                        }).display();*/
                break;
            case R.id.to_date_alms:
                selected = "2";

                /*new SingleDateAndTimePickerDialog.Builder(context)
                        .bottomSheet()
                        .curved()
                        // .minutesStep(15)
                        .displayHours(false)
                        .displayMinutes(false)
                        //.todayText("aujourd'hui")
                        .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                            @Override
                            public void onDisplayed(SingleDateAndTimePicker picker) {
                                //retrieve the SingleDateAndTimePicker
                            }
                        })

                        .title("Select Date")
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date) {
                               *//* Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);

                                endDateTime.setText(dateTime);*//*


                                Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                SimpleDateFormat dateFormat_new = new SimpleDateFormat("MM/dd/yyyy");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);

                                if (!fromDateAlms.getText().toString().isEmpty()) {
                                    boolean b = CheckDates(fromDateAlms.getText().toString(), dateTime);

                                    if (b) {
                                        t_d_alms = dateFormat_new.format(date);
                                        toDateAlms.setText(dateTime);
                                        GET_BALANCE_FILTER(f_d_alms,
                                                t_d_alms, "4");
                                    } else {
                                        t_d_alms = "";
                                        toDateAlms.setText("");
                                        util.IOSDialog(context, "To date should be greater then from date.");
                                    }
                                } else {
                                    util.IOSDialog(context, "Please select from date first");
                                }

                            }
                        }).display();*/
                break;
            case R.id.from_date_tithe:

                selected = "3";

             /*   new SingleDateAndTimePickerDialog.Builder(context)
                        .bottomSheet()
                        .curved()
                        // .minutesStep(15)
                        .displayHours(false)
                        .displayMinutes(false)
                        //.todayText("aujourd'hui")
                        .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                            @Override
                            public void onDisplayed(SingleDateAndTimePicker picker) {
                                //retrieve the SingleDateAndTimePicker
                            }
                        })

                        .title("Select Date")
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date) {
                               *//* Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);

                                endDateTime.setText(dateTime);*//*


                                Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                SimpleDateFormat dateFormat_new = new SimpleDateFormat("MM/dd/yyyy");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);


                                if (!toDateTithe.getText().toString().isEmpty()) {
                                    boolean b = CheckDates(dateTime, toDateTithe.getText().toString());

                                    if (b) {
                                        f_d_tithe = dateFormat_new.format(date);
                                        fromDateTithe.setText(dateTime);
                                        GET_BALANCE_FILTER(f_d_tithe,
                                                t_d_tithe, "2");
                                    } else {
                                        // fromDateTithe.setText("");
                                        util.IOSDialog(context, "From date should be lesser then to date.");
                                    }
                                } else {
                                    f_d_tithe = dateFormat_new.format(date);
                                    fromDateTithe.setText(dateTime);

                                }


                            }
                        }).display();*/

                break;
            case R.id.to_date_tithe:
                selected = "4";
               /* new SingleDateAndTimePickerDialog.Builder(context)
                        .bottomSheet()
                        .curved()
                        // .minutesStep(15)
                        .displayHours(false)
                        .displayMinutes(false)
                        //.todayText("aujourd'hui")
                        .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                            @Override
                            public void onDisplayed(SingleDateAndTimePicker picker) {
                                //retrieve the SingleDateAndTimePicker
                            }
                        })

                        .title("Select Date")
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date) {
                               *//* Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);

                                endDateTime.setText(dateTime);*//*


                                Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                SimpleDateFormat dateFormat_new = new SimpleDateFormat("MM/dd/yyyy");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);

                                if (!fromDateTithe.getText().toString().isEmpty()) {
                                    boolean b = CheckDates(fromDateTithe.getText().toString(), dateTime);

                                    if (b) {
                                        t_d_tithe = dateFormat_new.format(date);
                                        toDateTithe.setText(dateTime);
                                        GET_BALANCE_FILTER(f_d_tithe,
                                                t_d_tithe, "2");
                                    } else {
                                        t_d_tithe = "";
                                        toDateTithe.setText("");
                                        util.IOSDialog(context, "To date should be greater then from date.");
                                    }
                                } else {
                                    util.IOSDialog(context, "Please select from date first");
                                }

                            }
                        }).display();*/
                break;
            case R.id.from_date_seeds:
                selected = "5";
               /* new SingleDateAndTimePickerDialog.Builder(context)
                        .bottomSheet()
                        .curved()
                        // .minutesStep(15)
                        .displayHours(false)
                        .displayMinutes(false)
                        //.todayText("aujourd'hui")
                        .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                            @Override
                            public void onDisplayed(SingleDateAndTimePicker picker) {
                                //retrieve the SingleDateAndTimePicker
                            }
                        })

                        .title("Select Date")
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date) {
                               *//* Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);

                                endDateTime.setText(dateTime);*//*


                                Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                SimpleDateFormat dateFormat_new = new SimpleDateFormat("MM/dd/yyyy");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);

                                if (!toDateSeeds.getText().toString().isEmpty()) {
                                    boolean b = CheckDates(dateTime, toDateSeeds.getText().toString());

                                    if (b) {
                                        f_d_seeds = dateFormat_new.format(date);
                                        fromDateSeeds.setText(dateTime);
                                        GET_BALANCE_FILTER(f_d_seeds,
                                                t_d_seeds, "1");
                                    } else {
                                        // fromDateTithe.setText("");
                                        util.IOSDialog(context, "From date should be lesser then to date.");
                                    }
                                } else {
                                    f_d_seeds = dateFormat_new.format(date);
                                    fromDateSeeds.setText(dateTime);

                                }


                            }
                        }).display();*/
                break;
            case R.id.to_date_seeds:
                selected = "6";
               /* new SingleDateAndTimePickerDialog.Builder(context)
                        .bottomSheet()
                        .curved()
                        // .minutesStep(15)
                        .displayHours(false)
                        .displayMinutes(false)
                        //.todayText("aujourd'hui")
                        .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                            @Override
                            public void onDisplayed(SingleDateAndTimePicker picker) {
                                //retrieve the SingleDateAndTimePicker
                            }
                        })

                        .title("Select Date")
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date) {
                               *//* Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);

                                endDateTime.setText(dateTime);*//*


                                Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                SimpleDateFormat dateFormat_new = new SimpleDateFormat("MM/dd/yyyy");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);


                                if (!fromDateSeeds.getText().toString().isEmpty()) {
                                    boolean b = CheckDates(fromDateSeeds.getText().toString(), dateTime);

                                    if (b) {
                                        t_d_seeds = dateFormat_new.format(date);
                                        toDateSeeds.setText(dateTime);
                                        GET_BALANCE_FILTER(f_d_seeds,
                                                t_d_seeds, "1");
                                    } else {
                                        toDateSeeds.setText("");
                                        util.IOSDialog(context, "To date should be greater then from date.");
                                    }
                                } else {
                                    util.IOSDialog(context, "Please select from date first");
                                }


                            }
                        }).display();*/
                break;
            case R.id.from_date_withdrawals:
                selected = "7";
               /* new SingleDateAndTimePickerDialog.Builder(context)
                        .bottomSheet()
                        .curved()
                        // .minutesStep(15)
                        .displayHours(false)
                        .displayMinutes(false)
                        //.todayText("aujourd'hui")
                        .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                            @Override
                            public void onDisplayed(SingleDateAndTimePicker picker) {
                                //retrieve the SingleDateAndTimePicker
                            }
                        })

                        .title("Select Date")
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date) {
                               *//* Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);

                                endDateTime.setText(dateTime);*//*


                                Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                SimpleDateFormat dateFormat_new = new SimpleDateFormat("MM/dd/yyyy");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);
                                if (!toDateWithdrawals.getText().toString().isEmpty()) {
                                    boolean b = CheckDates(dateTime, toDateWithdrawals.getText().toString());

                                    if (b) {
                                        f_d_withdrawal = dateFormat_new.format(date);
                                        fromDateWithdrawals.setText(dateTime);
                                        GET_BALANCE_FILTER(f_d_withdrawal,
                                                t_d_withdrawal, "3");
                                    } else {
                                        // fromDateTithe.setText("");
                                        util.IOSDialog(context, "From date should be lesser then to date.");
                                    }
                                } else {
                                    f_d_withdrawal = dateFormat_new.format(date);
                                    fromDateWithdrawals.setText(dateTime);

                                }


                            }
                        }).display();*/
                break;
            case R.id.to_date_withdrawals:
                selected = "8";
               /* new SingleDateAndTimePickerDialog.Builder(context)
                        .bottomSheet()
                        .curved()
                        // .minutesStep(15)
                        .displayHours(false)
                        .displayMinutes(false)
                        //.todayText("aujourd'hui")
                        .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                            @Override
                            public void onDisplayed(SingleDateAndTimePicker picker) {
                                //retrieve the SingleDateAndTimePicker
                            }
                        })

                        .title("Select Date")
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date) {
                               *//* Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);

                                endDateTime.setText(dateTime);*//*


                                Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                SimpleDateFormat dateFormat_new = new SimpleDateFormat("MM/dd/yyyy");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);

                                if (!fromDateWithdrawals.getText().toString().isEmpty()) {
                                    boolean b = CheckDates(fromDateWithdrawals.getText().toString(), dateTime);

                                    if (b) {
                                        t_d_withdrawal = dateFormat_new.format(date);
                                        toDateWithdrawals.setText(dateTime);
                                        GET_BALANCE_FILTER(f_d_withdrawal,
                                                t_d_withdrawal, "3");
                                    } else {
                                        toDateWithdrawals.setText("");
                                        util.IOSDialog(context, "To date should be greater then from date.");
                                    }
                                } else {
                                    util.IOSDialog(context, "Please select from date first");
                                }


                            }
                        }).display();*/
                break;
        }
    }

    private void GET_BALANCE_FILTER(String from_date, String to_date, String type) {
        Log.e("dateee", from_date + "-" + to_date + "-" + type);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.GET_BALANCE_FILTER
                + "?from_date=" + from_date + "&to_date=" + to_date + "&type=" + type, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {

                           // String s = truncateUptoTwoDecimal(Double.parseDouble(jsonmainObject.getJSONObject("data").getString("balance")));
                          //  String bd = String.format("%.2f", jsonmainObject.getJSONObject("data").getString("balance"));
                            String bd = jsonmainObject.getJSONObject("data").getString("balance");
                         //   BigDecimal bd1 = new BigDecimal(s.replace(",", "."));
                           // BigDecimal bd = new BigDecimal(bd1.stripTrailingZeros().toPlainString());


                            if (type.equals("2")) {
                                titheAmount.setText(savePref.getCurrency() + util.getDecimalFormattedString(String.valueOf(bd)));
                            } else if (type.equals("1")) {
                                seedsAmount.setText(savePref.getCurrency() + util.getDecimalFormattedString(String.valueOf(bd)));
                            } else if (type.equals("4")) {
                                almsAmount.setText(savePref.getCurrency() + util.getDecimalFormattedString(String.valueOf(bd)));
                            } else {
                                withdrawalAmount.setText(savePref.getCurrency() + util.getDecimalFormattedString(String.valueOf(bd)));
                            }


                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    public String truncateUptoTwoDecimal(double doubleValue) {
        String value = String.valueOf(doubleValue);
        if (value != null) {
            String result = value;
            int decimalIndex = result.indexOf(".");
            if (decimalIndex != -1) {
                String decimalString = result.substring(decimalIndex + 1);
                if (decimalString.length() > 2) {
                    result = value.substring(0, decimalIndex + 3);
                } else if (decimalString.length() == 1) {
                    result = String.format(Locale.ENGLISH, "%.2f",
                            Double.parseDouble(value));
                }
            }
            return result;
        }
        return null;
    }


    public static boolean CheckDates(String d1, String d2) {
        SimpleDateFormat dfDate = new SimpleDateFormat("dd/MM/yyyy");
        boolean b = false;
        try {
            if (dfDate.parse(d1).before(dfDate.parse(d2))) {
                b = true;//If start date is before end date
            } else if (dfDate.parse(d1).equals(dfDate.parse(d2))) {
                b = false;//If two dates are equal
            } else {
                b = false; //If start date is after the end date
            }
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return b;
    }
}
