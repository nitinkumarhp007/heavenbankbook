package com.heaven_bank_book.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.heaven_bank_book.Models.TransectionModel;
import com.heaven_bank_book.R;
import com.heaven_bank_book.Util.ConnectivityReceiver;
import com.heaven_bank_book.Util.Parameters;
import com.heaven_bank_book.Util.SavePref;
import com.heaven_bank_book.Util.util;
import com.heaven_bank_book.parser.AllAPIS;
import com.heaven_bank_book.parser.GetAsync;
import com.heaven_bank_book.parser.GetAsyncPut;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SeedsActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    SeedsActivity context;
    @BindView(R.id.currency_1)
    TextView currency1;
    @BindView(R.id.currency_2)
    TextView currency2;
    @BindView(R.id.currency_3)
    TextView currency3;
    @BindView(R.id.currency_4)
    TextView currency4;
    private SavePref savePref;
    @BindView(R.id.church_name)
    EditText churchName;
    @BindView(R.id.return_expection)
    EditText returnExpection;
    @BindView(R.id.harvest_value)
    TextView harvestValue;
    @BindView(R.id.earth_measure)
    EditText earthMeasure;
    @BindView(R.id.heaven_measure)
    TextView heavenMeasure;
    @BindView(R.id.faith_percentage_balance)
    TextView faithPercentageBalance;
    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.date)
    TextView date_;

    TransectionModel transectionModel = null;
    int heaven_measure_amount = 0;
    boolean is_from_history = false;

    /* harvest_value=in*/
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seeds);
        ButterKnife.bind(this);

        is_from_history = getIntent().getBooleanExtra("is_from_history", false);

        date_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        SeedsActivity.this,
                        now.get(Calendar.YEAR), // Initial year selection
                        now.get(Calendar.MONTH), // Initial month selection
                        now.get(Calendar.DAY_OF_MONTH) // Inital day selection
                );
// If you're calling this from a support Fragment
                dpd.show(getSupportFragmentManager(), "Datepickerdialog");
            }
        });


        if (is_from_history) {
            transectionModel = getIntent().getExtras().getParcelable("model");

            submit.setVisibility(View.GONE);

            churchName.setText(transectionModel.getChurch_name());
            returnExpection.setText(util.getDecimalFormattedString(transectionModel.getReturn_expection()));
            earthMeasure.setText(util.getDecimalFormattedString(transectionModel.getEarth_measure()));
            harvestValue.setText(util.getDecimalFormattedString(transectionModel.getBalance_based_per()));
            heavenMeasure.setText(transectionModel.getHeaven_measure());
            faithPercentageBalance.setText(util.getDecimalFormattedString(transectionModel.getAmount()));
            description.setText(transectionModel.getDescription());
            date_.setText(util.convertTimeStampDateTime(Long.parseLong(transectionModel.getDate())));

            //   heaven_measure_amount = Integer.parseInt();

            if (transectionModel.getHeaven_measure().equals("30 fold")) {
                heaven_measure_amount = 30;
            } else if (transectionModel.getHeaven_measure().equals("60 fold")) {
                heaven_measure_amount = 60;
            } else if (transectionModel.getHeaven_measure().equals("100 fold")) {
                heaven_measure_amount = 100;
            }


            submit.setVisibility(View.VISIBLE);

            submit.setText("Update");

            /*churchName.setEnabled(false);
            returnExpection.setEnabled(false);
            earthMeasure.setEnabled(false);
            harvestValue.setEnabled(false);
            heavenMeasure.setEnabled(false);
            faithPercentageBalance.setEnabled(false);
            description.setEnabled(false);*/

        }
        context = SeedsActivity.this;
        savePref = new SavePref(context);

        currency1.setText(savePref.getCurrency());
        currency2.setText(savePref.getCurrency());
        currency3.setText(savePref.getCurrency());
        currency4.setText(savePref.getCurrency());


        setToolbar();

        earthMeasure.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (heaven_measure_amount > 0 && !earthMeasure.getText().toString().isEmpty()) {
                    setvalue();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    earthMeasure.removeTextChangedListener(this);
                    String value = earthMeasure.getText().toString();


                    if (value != null && !value.equals("")) {

                        if (value.startsWith(".")) {
                            earthMeasure.setText("0.");
                        }
                        if (value.startsWith("0") && !value.startsWith("0.")) {
                            earthMeasure.setText("");

                        }


                        String str = earthMeasure.getText().toString().replaceAll(",", "");
                        if (!value.equals(""))
                            earthMeasure.setText(util.getDecimalFormattedString(str));
                        earthMeasure.setSelection(earthMeasure.getText().toString().length());
                    }
                    earthMeasure.addTextChangedListener(this);
                    return;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    earthMeasure.addTextChangedListener(this);
                }

            }
        });

        returnExpection.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    returnExpection.removeTextChangedListener(this);
                    String value = returnExpection.getText().toString();


                    if (value != null && !value.equals("")) {

                        if (value.startsWith(".")) {
                            returnExpection.setText("0.");
                        }
                        if (value.startsWith("0") && !value.startsWith("0.")) {
                            returnExpection.setText("");

                        }


                        String str = returnExpection.getText().toString().replaceAll(",", "");
                        if (!value.equals(""))
                            returnExpection.setText(util.getDecimalFormattedString(str));
                        returnExpection.setSelection(returnExpection.getText().toString().length());
                    }
                    returnExpection.addTextChangedListener(this);
                    return;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    returnExpection.addTextChangedListener(this);
                }

            }
        });
        harvestValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    harvestValue.removeTextChangedListener(this);
                    String value = harvestValue.getText().toString();


                    if (value != null && !value.equals("")) {

                        if (value.startsWith(".")) {
                            harvestValue.setText("0.");
                        }
                        if (value.startsWith("0") && !value.startsWith("0.")) {
                            harvestValue.setText("");

                        }


                        String str = harvestValue.getText().toString().replaceAll(",", "");
                        if (!value.equals(""))
                            harvestValue.setText(util.getDecimalFormattedString(str));
                        //grossIncome.setSelection(grossIncome.getText().toString().length());
                    }
                    harvestValue.addTextChangedListener(this);
                    return;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    harvestValue.addTextChangedListener(this);
                }

            }
        });
        faithPercentageBalance.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    faithPercentageBalance.removeTextChangedListener(this);
                    String value = faithPercentageBalance.getText().toString();


                    if (value != null && !value.equals("")) {

                        if (value.startsWith(".")) {
                            faithPercentageBalance.setText("0.");
                        }
                        if (value.startsWith("0") && !value.startsWith("0.")) {
                            faithPercentageBalance.setText("");

                        }


                        String str = faithPercentageBalance.getText().toString().replaceAll(",", "");
                        if (!value.equals(""))
                            faithPercentageBalance.setText(util.getDecimalFormattedString(str));
                        //grossIncome.setSelection(grossIncome.getText().toString().length());
                    }
                    faithPercentageBalance.addTextChangedListener(this);
                    return;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    faithPercentageBalance.addTextChangedListener(this);
                }

            }
        });


    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = "You picked the following date: " + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
        // endDateTime.setText(dateTime);

        String date_text = String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(year);

        date_.setText(date_text);
    }


    private void setvalue() {
        if (!earthMeasure.getText().toString().replace(",", "").isEmpty()) {


            if (!earthMeasure.getText().toString().contains(".")) {
                int cal_amount = Integer.valueOf(heaven_measure_amount) * Integer.valueOf(earthMeasure.getText().toString().replace(",", ""));
                harvestValue.setText(String.valueOf(cal_amount));
                faithPercentageBalance.setText(String.valueOf(cal_amount));
            } else {
                //  double cal_amount = Double.valueOf(heaven_measure_amount) * Double.valueOf(earthMeasure.getText().toString().replace(",", ""));

                double dd = new BigDecimal(heaven_measure_amount).doubleValue();
                String val = String.format("%.4f", dd);
                BigDecimal bd111 = BigDecimal.valueOf(Double.valueOf(val));
                Double d = bd111.doubleValue();

               /* DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
                df.setMaximumFractionDigits(340);
                double v1= Double.parseDouble(df.format(d));*/



                double dd1 = new BigDecimal(earthMeasure.getText().toString().replace(",", "")).doubleValue();
                String val1 = String.format("%.4f", dd1);
                BigDecimal bd11 = BigDecimal.valueOf(Double.valueOf(val1));
                Double d11 = bd11.doubleValue();


               // double v2= Double.parseDouble(df.format(d11));

                double cal_amount = d * d11;

                String s = String.format("%.2f", cal_amount);
                BigDecimal bd1 = new BigDecimal(s.replace(",", "."));
                BigDecimal bd = new BigDecimal(bd1.stripTrailingZeros().toPlainString());

                harvestValue.setText(String.valueOf(bd));
                faithPercentageBalance.setText(String.valueOf(bd));


            }


            // faithPercentageBalance.setText(String.format("%.2f", cal_amount));
        }

    }

    public String truncateUptoTwoDecimal(double doubleValue) {
        String value = String.valueOf(doubleValue);
        if (value != null) {
            String result = value;
            int decimalIndex = result.indexOf(".");
            if (decimalIndex != -1) {
                String decimalString = result.substring(decimalIndex + 1);
                if (decimalString.length() > 2) {
                    result = value.substring(0, decimalIndex + 3);
                } else if (decimalString.length() == 1) {
                    result = String.format(Locale.ENGLISH, "%.2f",
                            Double.parseDouble(value));
                }
            }
            return result;
        }
        return null;
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Seeds");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick({R.id.heaven_measure, R.id.submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.heaven_measure:
                util.hideKeyboard(context);
                //Creating the instance of PopupMenu
                PopupMenu popup = new PopupMenu(SeedsActivity.this, heavenMeasure);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.menu_main, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().equals("30 fold")) {
                            heaven_measure_amount = 30;
                            heavenMeasure.setText("30 fold");
                            setvalue();
                        } else if (item.getTitle().equals("60 fold")) {
                            heaven_measure_amount = 60;
                            heavenMeasure.setText("60 fold");
                            setvalue();
                        } else if (item.getTitle().equals("100 fold")) {
                            heaven_measure_amount = 100;
                            heavenMeasure.setText("100 fold");
                            setvalue();
                        }
                        return true;
                    }
                });

                popup.show();//showing popup menu
                break;
            case R.id.submit:
                if (ConnectivityReceiver.isConnected()) {
                    if (churchName.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Beneficiary Name");
                    } else if (returnExpection.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Return Expection");
                    } else if (earthMeasure.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Earth Measure");
                    } else if (heaven_measure_amount == 0) {
                        util.IOSDialog(context, "Please select heaven Measure");
                    } else if (date_.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Select Date");
                    } else if (description.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Enter description");
                    } else {
                        Alert();
                    }
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
        }
    }

    public void Alert() {
        new IOSDialog.Builder(context)
                .setMessage("Are you sure to Submit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!is_from_history)
                            ADD_SEED_API();
                        else
                            Edit_ADD_SEED_API();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void ADD_SEED_API() {
        Log.e("date____","sent"+util.date_to_timestamp(date_.getText().toString().replace(",", "").trim()));
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (is_from_history)
            formBuilder.addFormDataPart(Parameters.SEED_ID, transectionModel.getId());
        formBuilder.addFormDataPart(Parameters.CHURCH_NAME, churchName.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.DATE, util.date_to_timestamp(date_.getText().toString().replace(",", "").trim()));
        formBuilder.addFormDataPart(Parameters.RETURN_EXPECTION, returnExpection.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.EARTH_MEASURE, earthMeasure.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.HEAVEN_MEASURE, heavenMeasure.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.BALANCE_BASED_PER, harvestValue.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.AMOUNT, faithPercentageBalance.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, description.getText().toString().replace(",", "").trim());
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.ADD_SEED, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            finish();
                            if (is_from_history)
                                util.showToast(context, "SEED Updated Sucessfully!!!");
                            else
                                util.showToast(context, "SEED Added Sucessfully!!!");
                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.IOSDialog(context, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void Edit_ADD_SEED_API() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (is_from_history)
            formBuilder.addFormDataPart(Parameters.SEED_ID, transectionModel.getId());
        formBuilder.addFormDataPart(Parameters.CHURCH_NAME, churchName.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.RETURN_EXPECTION, returnExpection.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.EARTH_MEASURE, earthMeasure.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.HEAVEN_MEASURE, heavenMeasure.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.BALANCE_BASED_PER, harvestValue.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.AMOUNT, faithPercentageBalance.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, description.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.DATE, util.date_to_timestamp(date_.getText().toString().replace(",", "").trim()));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.ADD_SEED, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            Intent intent = new Intent();
                            intent.putExtra("type", getIntent().getStringExtra("type"));
                            setResult(RESULT_OK, intent);
                            finish();
                            util.showToast(context, "SEED Updated Sucessfully!!!");
                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.IOSDialog(context, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

}
