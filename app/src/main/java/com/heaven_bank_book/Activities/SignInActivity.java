package com.heaven_bank_book.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.heaven_bank_book.MainActivity;
import com.heaven_bank_book.R;
import com.heaven_bank_book.Util.ConnectivityReceiver;
import com.heaven_bank_book.Util.Parameters;
import com.heaven_bank_book.Util.SavePref;
import com.heaven_bank_book.Util.util;
import com.heaven_bank_book.parser.AllAPIS;
import com.heaven_bank_book.parser.GetAsync;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class SignInActivity extends AppCompatActivity {
    SignInActivity context;
    private SavePref savePref;
    @BindView(R.id.email_address)
    EditText emailAddress;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.sign_in)
    Button signIn;
    @BindView(R.id.forgot_password)
    Button forgotPassword;
    @BindView(R.id.sign_up)
    Button signUp;

    boolean is_in_app = false;

    CallbackManager callbackManager;
    String fb_occupation = "", fb_username = "", fb_phone = "", fb_email = "", fb_image = "", social_id = "",
            fb_gender = "", fb_dob = "", fb_firstname = "", fb_lastname = "", socail_id = "";
    ProgressDialog mDialog;

    private static final int RC_SIGN_IN = 0;
    @BindView(R.id.Facebookbutton)
    LoginButton Facebookbutton;
    @BindView(R.id.google)
    ImageView google;
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions googleSignInOptions;
    String auth_key = "";


   /* // To store the purchased items
    List<Purchase> purchaseHistory = new ArrayList<>();

    boolean isPurchaseQueryPending;

    PurchaseHelper purchaseHelper;
    String TAG = "in_app_purchase";*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(this);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
        context = SignInActivity.this;
        savePref = new SavePref(context);

        mDialog = util.initializeProgress(context);

        Log.e("device_token_", SavePref.getDeviceToken(this, "token"));

        callbackManager = CallbackManager.Factory.create();
        //googleplus login :)
        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        //Initializing google api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();


        Facebookbutton.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);

        if (ConnectivityReceiver.isConnected()) {
            facebookLogin();
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }


        /*purchaseHelper = new PurchaseHelper(this, getPurchaseHelperListener());


    }

    *//**
         * Your listener to handle the various responses of the Purchase helper util
         *//*
    public PurchaseHelper.PurchaseHelperListener getPurchaseHelperListener() {
        return new PurchaseHelper.PurchaseHelperListener() {
            @Override
            public void onServiceConnected(int resultCode) {
                Log.d(TAG, "onServiceConnected: " + resultCode);

                if (isPurchaseQueryPending) {
                    // purchaseHelper.getPurchasedItems(BillingClient.SkuType.SUBS);
                    isPurchaseQueryPending = false;
                }

            }

            @Override
            public void onSkuQueryResponse(List<SkuDetails> skuDetails) {
                //cityListingAdapter.setSkuList(skuDetails);
            }

            @Override
            public void onPurchasehistoryResponse(List<Purchase> purchasedItems) {
                purchaseHistory = purchasedItems;
                Log.e("yes", "No");

                if (purchaseHistory.size() == 0) {
                    purchaseHelper.launchBillingFLow(BillingClient.SkuType.INAPP, "android.test.purchased");
                    //  purchaseHelper.launchBillingFLow(BillingClient.SkuType.SUBS, "monthly_1_20");


                }
            }

            @Override
            public void onPurchasesUpdated(int responseCode, @Nullable List<Purchase> purchases) {
                purchaseHistory = purchases;
                if (purchaseHistory != null) {
                    if (purchaseHistory.size() > 0) {
                        util.IOSDialog(context, "Sucessfully");
                    }
                }


                Log.d(TAG, "onPurchasesUpdated: " + responseCode);


            }
        };*/


    }


    @OnClick({R.id.sign_in, R.id.forgot_password, R.id.google, R.id.sign_up})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.sign_in:
                SigninProcess();
                break;
            case R.id.forgot_password:
                startActivity(new Intent(this, ForgotPasswordActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.google:
                is_in_app = false;
                signInWithGplus();
                break;
            case R.id.sign_up:
                startActivity(new Intent(this, SignupActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }

    private void SigninProcess() {
        if (ConnectivityReceiver.isConnected()) {
            if (emailAddress.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Email Address");
                signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (!util.isValidEmail(emailAddress.getText().toString().trim())) {
                util.IOSDialog(context, "Please Enter a Vaild Email Address");
                signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else if (password.getText().toString().trim().isEmpty()) {
                util.IOSDialog(context, "Please Enter Password");
                signIn.startAnimation(AnimationUtils.loadAnimation(this, R.anim.shake));
            } else {
                LOGIN_API();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void LOGIN_API() {
        util.hideKeyboard(context);
        //Log.e("device_token__", SavePref.getDeviceToken(LoginActivity.this, "token"));
        //String s = SavePref.getDeviceToken(LoginActivity.this, "token");
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.EMAIL, emailAddress.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.PASSWORD, password.getText().toString().trim());
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(SignInActivity.this, "token"));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.USERLOGIN, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            JSONObject body = null;
                            try {
                                body = jsonMainobject.getJSONObject("data");
                                savePref.setEmail(body.getString("email"));
                                savePref.setPhone(body.optString("phone"));
                                savePref.setID(body.getString("id"));
                                savePref.setName(body.getString("name"));
                                savePref.setImage(body.getString("profile"));
                                savePref.setCountry(body.getString("country"));
                                if (!body.getString("country_code").isEmpty())
                                    savePref.setCurrency(util.getCurrencySymbol(body.getString("country_code")));
                                savePref.setIs_soical(false);

                                auth_key = body.getString("authorization_key");
                                savePref.setAuthorization_key(auth_key);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            is_in_app = true;
//                            signInWithGplus();

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.putExtra("auth_key", auth_key);
                            startActivity(intent);
                            finish();

                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }




    public void facebookLogin() {
        Facebookbutton.setReadPermissions(Arrays.asList("public_profile", "email", "user_birthday",
                "user_friends", "user_photos"));
        Facebookbutton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                mDialog.setMessage("please wait.....");
                mDialog.show();
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {

                                try {
                                    fb_email = object.getString("email");
                                    savePref.setEmail(fb_email);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    social_id = object.getString("id");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_image = "http://graph.facebook.com/" + socail_id + "/picture?type=large";
                                    savePref.setImage(fb_image);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                   /* fb_cover_image = "https://graph.facebook.com/"
                                            + socail_id + "?fields=cover&access_token=" + getCurrentAccessToken().getToken();*/

                                    JSONObject jsonObject = new JSONObject(response.toString());
                                    JSONObject sourceObj = jsonObject.getJSONObject("cover");
                                    String source = sourceObj.getString("source");
                                    // savePref.setCover_pic(source);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_username = object.getString("name");
                                    savePref.setName(fb_username);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_firstname = object.getString("first_name");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_lastname = object.getString("last_name");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_gender = object.getString("gender");
                                    fb_gender = fb_gender.substring(0, 1).toUpperCase()
                                            + fb_gender.substring(1, fb_gender.length());
                                    savePref.setGender(fb_gender);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    fb_dob = object.getString("birthday");
                                    //savePref.setDOB(fb_dob);

                                } catch (JSONException e) {
                                    e.printStackTrace();

                                }
                                socialLoginApi(fb_firstname + " " + fb_lastname, "1", fb_email);
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,first_name,last_name,email,gender,birthday");
                request.setParameters(parameters);
                request.executeAsync();
                mDialog.dismiss();
            }

            @Override
            public void onCancel() {
                mDialog.dismiss();
            }


            @Override
            public void onError(FacebookException error) {
                mDialog.dismiss();
            }

        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        LoginManager.getInstance().logOut();
    }

    private void socialLoginApi(final String NAME, final String SOCIAL_TYPE, final String EMAIL) {
        String email_address = "";
        if (EMAIL.isEmpty()) {
            email_address = socail_id + "@gmail.com";
        } else {
            email_address = EMAIL;
        }
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.SOCIAL_ID, social_id);
        formBuilder.addFormDataPart(Parameters.SOCIAL_TYPE, SOCIAL_TYPE);
        formBuilder.addFormDataPart(Parameters.NAME, NAME);
        formBuilder.addFormDataPart(Parameters.EMAIL, email_address);
        formBuilder.addFormDataPart(Parameters.DEVICE_TYPE, "1");
        formBuilder.addFormDataPart(Parameters.DEVICE_TOKEN, SavePref.getDeviceToken(this, "token"));
        formBuilder.addFormDataPart("social_token", SavePref.getDeviceToken(this, "token"));
        RequestBody formBody = formBuilder.build();
        String finalEmail_address = email_address;
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.SOCIAL_LOGIN, formBody, "") {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            JSONObject body = jsonMainobject.getJSONObject("data");
                            savePref.setEmail(body.getString("email"));
                            savePref.setID(body.getString("id"));
                            savePref.setName(body.getString("name"));
                            savePref.setImage(body.optString("profile"));
                            savePref.setImage(body.optString("phone"));
                            savePref.setIs_soical(true);
                            savePref.setCurrency("ZAR");

                            auth_key = body.getString("authorization_key");
                            if (SOCIAL_TYPE.equals("1")) {
                                is_in_app = true;
//                                signInWithGplus();
                                goToMainScreen();
                                
                            } else {

//                                IN_APP_API(finalEmail_address);

                                goToMainScreen();
                            }


                        } else {
                            util.IOSDialog(context, jsonMainobject.getString("error_message"));
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    //Google Plus Login
    //This function will option signing intent
    private void signInWithGplus() {
        try {
            mDialog.setMessage("please wait.....");
            if (!is_in_app)
                mDialog.show();
        } catch (Exception e) {

        }

        //Creating an intent
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    //For both google & fb login
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //for fb login
        callbackManager.onActivityResult(requestCode, resultCode, data);


        //for google+ login
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //Calling a new function to handle signin
            handleSignInResult(result);

        }/* else if (resultCode == RESULT_OK) {
            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String dataSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

            if (resultCode == RESULT_OK) {
                //Purchased Status == Purchased
                try {
                    //Product Details JSON
                    JSONObject object = new JSONObject(purchaseData);

                    //Purchased Product ID
                    String sku = object.getString("productId");
                    Log.e("data", "======" + sku + "  " + purchaseData);
                } catch (JSONException e) {

                    e.printStackTrace();
                }
            } else {
                Log.e("errorrrrrrrrrr", "====");
                //Purchased Status = Not Purchased
            }
        }*/

    }

    //After the signing we are calling this function
    private void handleSignInResult(GoogleSignInResult result) {

        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();
            social_id = acct.getId();
            fb_username = acct.getGivenName();
            fb_email = acct.getEmail();
            Uri personPhoto = acct.getPhotoUrl();
            String username = acct.getDisplayName();
            if (!is_in_app) {
                socialLoginApi(fb_username, "2", fb_email);
                mDialog.setMessage("please wait.....");
                mDialog.show();
            } else {

//                IN_APP_API(fb_email);

                goToMainScreen();
            }

        } else {
            //If login fails
            mDialog.show();
            Toast.makeText(this, "" + result.getStatus().toString(), Toast.LENGTH_LONG).show();
        }
    }

    private void goToMainScreen() {
        savePref.setAuthorization_key(auth_key);
        Intent intent = new Intent(context, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
        util.showToast(context, "Signup Sucessfully!!!");
    }

}

