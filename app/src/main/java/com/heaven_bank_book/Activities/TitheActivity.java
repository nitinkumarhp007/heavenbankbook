package com.heaven_bank_book.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.heaven_bank_book.Models.TransectionModel;
import com.heaven_bank_book.R;
import com.heaven_bank_book.Util.ConnectivityReceiver;
import com.heaven_bank_book.Util.Parameters;
import com.heaven_bank_book.Util.SavePref;
import com.heaven_bank_book.Util.util;
import com.heaven_bank_book.parser.AllAPIS;
import com.heaven_bank_book.parser.GetAsync;
import com.heaven_bank_book.parser.GetAsyncPut;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class TitheActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    TitheActivity context;
    @BindView(R.id.currency_1)
    TextView currency1;
    @BindView(R.id.currency_2)
    TextView currency2;
    @BindView(R.id.currency_3)
    TextView currency3;
    @BindView(R.id.currency_4)
    TextView currency4;
    @BindView(R.id.petitions)
    EditText petitions;
    private SavePref savePref;
    @BindView(R.id.church_name)
    EditText churchName;
    @BindView(R.id.turnover)
    EditText turnover;
    @BindView(R.id.cost_of_sales)
    EditText costOfSales;
    @BindView(R.id.gross_income)
    TextView grossIncome;
    @BindView(R.id.tithe)
    TextView tithe;
    @BindView(R.id.date)
    TextView date_;


    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.submit)
    Button submit;

    boolean is_from_history = false;

    TransectionModel transectionModel = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tithe);
        ButterKnife.bind(this);
        setToolbar();
        is_from_history = getIntent().getBooleanExtra("is_from_history", false);

        date_.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* new SingleDateAndTimePickerDialog.Builder(context)
                        .bottomSheet()
                        .curved()
                        // .minutesStep(15)
                        .displayHours(false)
                        .displayMinutes(false)
                        //.todayText("aujourd'hui")
                        .displayListener(new SingleDateAndTimePickerDialog.DisplayListener() {
                            @Override
                            public void onDisplayed(SingleDateAndTimePicker picker) {
                                //retrieve the SingleDateAndTimePicker
                            }
                        })

                        .title("Select Date")
                        .listener(new SingleDateAndTimePickerDialog.Listener() {
                            @Override
                            public void onDateSelected(Date date) {

                                Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);

                                endDateTime.setText(dateTime);


                                Log.e("date___", date.toString());
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                                String dateTime = dateFormat.format(date);
                                Log.e("date___", dateTime);

                                date_.setText(dateTime);

                            }
                        }).display();*/

                Calendar now = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        TitheActivity.this,
                        now.get(Calendar.YEAR), // Initial year selection
                        now.get(Calendar.MONTH), // Initial month selection
                        now.get(Calendar.DAY_OF_MONTH) // Inital day selection
                );
// If you're calling this from a support Fragment
                dpd.show(getSupportFragmentManager(), "Datepickerdialog");
            }
        });


        if (is_from_history) {
            transectionModel = getIntent().getExtras().getParcelable("model");

            churchName.setText(transectionModel.getChurch_name());
            turnover.setText(util.getDecimalFormattedString(transectionModel.getTurn_over()));
            costOfSales.setText(util.getDecimalFormattedString(transectionModel.getSale_cost()));
            grossIncome.setText(util.getDecimalFormattedString(transectionModel.getGross_income()));
            tithe.setText(util.getDecimalFormattedString(transectionModel.getBalance()));
            description.setText(transectionModel.getDescription());
            date_.setText(util.convertTimeStampDateTime(Long.parseLong(transectionModel.getDate())));

            submit.setVisibility(View.VISIBLE);
            submit.setText("Update");

           /* description.setEnabled(false);
            churchName.setEnabled(false);
            turnover.setEnabled(false);
            costOfSales.setEnabled(false);
            grossIncome.setEnabled(false);
            tithe.setEnabled(false);*/

        }

        context = TitheActivity.this;
        savePref = new SavePref(context);


        currency1.setText(savePref.getCurrency());
        currency2.setText(savePref.getCurrency());
        currency3.setText(savePref.getCurrency());
        currency4.setText(savePref.getCurrency());


        tithe.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    tithe.removeTextChangedListener(this);
                    String value = tithe.getText().toString();


                    if (value != null && !value.equals("")) {

                        if (value.startsWith(".")) {
                            tithe.setText("0.");
                        }
                        if (value.startsWith("0") && !value.startsWith("0.")) {
                            tithe.setText("");

                        }


                        String str = tithe.getText().toString().replaceAll(",", "");
                        if (!value.equals(""))
                            tithe.setText(util.getDecimalFormattedString(str));
                          //  tithe.setText(String.format("%.3f", util.getDecimalFormattedString(str)));
                        //grossIncome.setSelection(grossIncome.getText().toString().length());
                    }
                    tithe.addTextChangedListener(this);
                    return;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    tithe.addTextChangedListener(this);
                }

            }
        });
        grossIncome.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    grossIncome.removeTextChangedListener(this);
                    String value = grossIncome.getText().toString();


                    if (value != null && !value.equals("")) {

                        if (value.startsWith(".")) {
                            grossIncome.setText("0.");
                        }
                        if (value.startsWith("0") && !value.startsWith("0.")) {
                            grossIncome.setText("");

                        }


                        String str = grossIncome.getText().toString().replaceAll(",", "");
                        if (!value.equals(""))
                            grossIncome.setText(util.getDecimalFormattedString(str));
                        //grossIncome.setSelection(grossIncome.getText().toString().length());
                    }
                    grossIncome.addTextChangedListener(this);
                    return;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    grossIncome.addTextChangedListener(this);
                }

            }
        });


        turnover.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence st, int start, int before, int count) {
                if (!st.toString().isEmpty())
                    calculation(true);

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    turnover.removeTextChangedListener(this);
                    String value = turnover.getText().toString();


                    if (value != null && !value.equals("")) {

                        if (value.startsWith(".")) {
                            turnover.setText("0.");
                        }
                        if (value.startsWith("0") && !value.startsWith("0.")) {
                            turnover.setText("");

                        }


                        String str = turnover.getText().toString().replaceAll(",", "");
                        if (!value.equals(""))
                            turnover.setText(util.getDecimalFormattedString(str));
                        turnover.setSelection(turnover.getText().toString().length());
                    }
                    turnover.addTextChangedListener(this);
                    return;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    turnover.addTextChangedListener(this);
                }


            }
        });
        //turnover.addTextChangedListener(new NumberTextWatcherForThousand(turnover));

        costOfSales.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().isEmpty())
                    calculation(false);
            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    costOfSales.removeTextChangedListener(this);
                    String value = costOfSales.getText().toString();


                    if (value != null && !value.equals("")) {

                        if (value.startsWith(".")) {
                            costOfSales.setText("0.");
                        }
                        if (value.startsWith("0") && !value.startsWith("0.")) {
                            costOfSales.setText("");

                        }


                        String str = costOfSales.getText().toString().replaceAll(",", "");
                        if (!value.equals(""))
                            costOfSales.setText(util.getDecimalFormattedString(str));
                        costOfSales.setSelection(costOfSales.getText().toString().length());
                    }
                    costOfSales.addTextChangedListener(this);
                    return;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    costOfSales.addTextChangedListener(this);
                }

            }
        });
        /*date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new MyDatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "date picker");
            }
        });*/

    }

    private void calculation(boolean revenue) {

        if (turnover.getText().toString().replace(",", "").isEmpty()) {
            costOfSales.setText("");
            grossIncome.setText("");
            tithe.setText("");
        } else if (costOfSales.getText().toString().replace(",", "").isEmpty()) {
            grossIncome.setText("");
            tithe.setText("");
        } else {
            //Gross income value bysubtracting total overheads from Revenue
            if (!turnover.getText().toString().replace(",", "").isEmpty() && !costOfSales.getText().toString().replace(",", "").isEmpty()) {
                if (Double.valueOf(turnover.getText().toString().replace(",", "")) > Double.valueOf(costOfSales.getText().toString().replace(",", ""))) {


                    if (!turnover.getText().toString().contains(".") && !costOfSales.getText().toString().contains(".")) {
                        int gross_income = Integer.valueOf(turnover.getText().toString().replace(",", "")) - Integer.valueOf(costOfSales.getText().toString().replace(",", ""));
                        int tithe_income = gross_income / 10;

                        grossIncome.setText(String.valueOf(gross_income));

                        tithe.setText(String.valueOf(tithe_income));
                    } else {

                        //revenue - totoal overhead

                        double dd = new BigDecimal(turnover.getText().toString().replace(",", "")).doubleValue();
                        String val = String.format("%.4f", dd);
                        BigDecimal bd111 = BigDecimal.valueOf(Double.valueOf(val));
                        Double d = bd111.doubleValue();

                        DecimalFormat df = new DecimalFormat("0", DecimalFormatSymbols.getInstance(Locale.ENGLISH));
                        df.setMaximumFractionDigits(340);
                        //double v1 = Double.parseDouble(df.format(d));


                        double dd1 = new BigDecimal(costOfSales.getText().toString().replace(",", "")).doubleValue();
                        String val1 = String.format("%.4f", dd1);
                        BigDecimal bd113 = BigDecimal.valueOf(Double.valueOf(val1));
                        Double d112 = bd113.doubleValue();

                        //double v2 = Double.parseDouble(df.format(d112));


                        //Double gross_income = Double.valueOf(turnover.getText().toString().replace(",", "")) - Double.valueOf(costOfSales.getText().toString().replace(",", ""));
                        Double gross_income = d - d112;

                        String ddddd = df.format(gross_income);
                        Log.e("dddddddd", ddddd);

                        Double tithe_income = gross_income / 10;

                        String fffff = df.format(tithe_income);

                        // String s1 = String.format("%.2f", gross_income);

                        /*String s1 = truncateUptoTwoDecimal(gross_income);
                        BigDecimal bd1 = new BigDecimal(s1.replace(",", "."));
                        BigDecimal bd11 = new BigDecimal(bd1.stripTrailingZeros().toPlainString());


                        // String s2 = String.format("%.2f", tithe_income);
                        String s2 = truncateUptoTwoDecimal(tithe_income);
                        BigDecimal bd2 = new BigDecimal(s2.replace(",", "."));
                        BigDecimal bd22 = new BigDecimal(bd2.stripTrailingZeros().toPlainString());*/


                        grossIncome.setText(String.valueOf(ddddd));

                        tithe.setText(String.valueOf(fffff));
                    }


                } else {
                    util.IOSDialog(context, "Total Overheads Cannot Be Greater Then Revenue");
                    costOfSales.setText("");
                    grossIncome.setText("");
                    tithe.setText("");

                }
            }
        }

    }

    public String truncateUptoTwoDecimal(double doubleValue) {
        String value = String.valueOf(doubleValue);
        if (value != null) {
            String result = value;
            int decimalIndex = result.indexOf(".");
            if (decimalIndex != -1) {
                String decimalString = result.substring(decimalIndex + 1);
                if (decimalString.length() > 2) {
                    result = value.substring(0, decimalIndex + 3);
                } else if (decimalString.length() == 1) {
                    result = String.format(Locale.ENGLISH, "%.2f",
                            Double.parseDouble(value));
                }
            }
            return result;
        }
        return null;
    }


    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText("Tithe");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @OnClick({R.id.submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.submit:
                if (ConnectivityReceiver.isConnected()) {
                    if (churchName.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Church Name");
                    } else if (turnover.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Turn over");
                    }/* else if (costOfSales.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Cost Of Sales");
                    }*//* else if (petitions.getText().toString().trim().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Petitions");
                    } */ else if (date_.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Select Date");
                    } else if (description.getText().toString().isEmpty()) {
                        util.IOSDialog(context, "Please Enter Description");
                    } else {
                        Alert();
                    }
                } else {
                    util.IOSDialog(context, util.internet_Connection_Error);
                }
                break;
        }
    }

    public void Alert() {
        new IOSDialog.Builder(context)
                .setMessage("Are you sure to Submit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (is_from_history)
                            Edit_ADD_TITHE_API();
                        else
                            ADD_TITHE_API();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

    private void ADD_TITHE_API() {

        String cost_of_sale = "";
        if (costOfSales.getText().toString().replace(",", "").trim().isEmpty()) {
            cost_of_sale = "0";
        } else {
            cost_of_sale = costOfSales.getText().toString().replace(",", "").trim();
        }
        String gross_income = "";
        if (grossIncome.getText().toString().replace(",", "").trim().isEmpty()) {
            gross_income = "0";
        } else {
            gross_income = grossIncome.getText().toString().replace(",", "").trim();
        }

        String balance = "";
        if (!tithe.getText().toString().replace(",", "").trim().isEmpty()) {
            balance = tithe.getText().toString().replace(",", "").trim();
        } else {
            balance = turnover.getText().toString().replace(",", "").trim();
        }

        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (is_from_history)
            formBuilder.addFormDataPart(Parameters.TITHE_ID, transectionModel.getId());
        formBuilder.addFormDataPart(Parameters.CHURCH_NAME, churchName.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.TURN_OVER, turnover.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.DATE, util.date_to_timestamp(date_.getText().toString().replace(",", "").trim()));
        formBuilder.addFormDataPart(Parameters.SALE_COST, cost_of_sale);
        formBuilder.addFormDataPart(Parameters.GROSS_INCOME, gross_income);
        formBuilder.addFormDataPart(Parameters.TIME, "0");
        formBuilder.addFormDataPart(Parameters.BALANCE, balance);
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, description.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.PETITIONS, "123");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.ADD_TITHE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            finish();

                            if (is_from_history)
                                util.showToast(context, "TITHE Updated Sucessfully!!!");
                            else
                                util.showToast(context, "TITHE Added Sucessfully!!!");

                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.IOSDialog(context, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private void Edit_ADD_TITHE_API() {

        String cost_of_sale = "";
        if (costOfSales.getText().toString().replace(",", "").trim().isEmpty()) {
            cost_of_sale = "0";
        } else {
            cost_of_sale = costOfSales.getText().toString().replace(",", "").trim();
        }
        String gross_income = "";
        if (grossIncome.getText().toString().replace(",", "").trim().isEmpty()) {
            gross_income = "0";
        } else {
            gross_income = grossIncome.getText().toString().replace(",", "").trim();
        }

        String balance = "";
        if (!tithe.getText().toString().replace(",", "").trim().isEmpty()) {
            balance = tithe.getText().toString().replace(",", "").trim();
        } else {
            balance = turnover.getText().toString().replace(",", "").trim();
        }

        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        if (is_from_history)
            formBuilder.addFormDataPart(Parameters.TITHE_ID, transectionModel.getId());
        formBuilder.addFormDataPart(Parameters.CHURCH_NAME, churchName.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.TURN_OVER, turnover.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.SALE_COST, cost_of_sale);
        formBuilder.addFormDataPart(Parameters.GROSS_INCOME, gross_income);
        formBuilder.addFormDataPart(Parameters.TIME, "0");
        formBuilder.addFormDataPart(Parameters.DATE, util.date_to_timestamp(date_.getText().toString().replace(",", "").trim()));
        formBuilder.addFormDataPart(Parameters.BALANCE, balance);
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, description.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.PETITIONS, "123");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncPut mAsync = new GetAsyncPut(context, AllAPIS.ADD_TITHE, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            Intent intent = new Intent();
                            intent.putExtra("type", getIntent().getStringExtra("type"));
                            setResult(RESULT_OK, intent);
                            finish();
                            util.showToast(context, "TITHE Updated Sucessfully!!!");

                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.IOSDialog(context, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        String date = "You picked the following date: " + dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
        // endDateTime.setText(dateTime);

        String date_text = String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(year);

        date_.setText(date_text);
    }
}
