package com.heaven_bank_book.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;

import com.heaven_bank_book.Models.TransectionModel;
import com.heaven_bank_book.R;
import com.heaven_bank_book.Util.ConnectivityReceiver;
import com.heaven_bank_book.Util.MyDatePickerFragment;
import com.heaven_bank_book.Util.Parameters;
import com.heaven_bank_book.Util.SavePref;
import com.heaven_bank_book.Util.util;
import com.heaven_bank_book.parser.AllAPIS;
import com.heaven_bank_book.parser.GetAsync;
import com.heaven_bank_book.parser.GetAsyncGet;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class WithdrawalActivity extends AppCompatActivity {
    WithdrawalActivity context;
    @BindView(R.id.currency_1)
    TextView currency1;
    private SavePref savePref;
    @BindView(R.id.current_balance)
    TextView currentBalance;
    @BindView(R.id.withdrawal)
    EditText withdrawal;

    public static TextView date;

    @BindView(R.id.description)
    EditText description;
    @BindView(R.id.submit)
    Button submit;
    double balance;
    boolean is_from_history = false;
    TransectionModel transectionModel = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdrawal);
        ButterKnife.bind(this);
        setToolbar();
        date = (TextView) findViewById(R.id.date);
        context = WithdrawalActivity.this;
        savePref = new SavePref(context);

        currency1.setText(savePref.getCurrency());

        is_from_history = getIntent().getBooleanExtra("is_from_history", false);

        if (is_from_history) {
            transectionModel = getIntent().getExtras().getParcelable("model");
        }

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new MyDatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "date picker");
            }
        });

        withdrawal.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    withdrawal.removeTextChangedListener(this);
                    String value = withdrawal.getText().toString();


                    if (value != null && !value.equals("")) {

                        if (value.startsWith(".")) {
                            withdrawal.setText("0.");
                        }
                        if (value.startsWith("0") && !value.startsWith("0.")) {
                            withdrawal.setText("");

                        }


                        String str = withdrawal.getText().toString().replaceAll(",", "");
                        if (!value.equals(""))
                            withdrawal.setText(util.getDecimalFormattedString(str));
                        withdrawal.setSelection(withdrawal.getText().toString().length());
                    }
                    withdrawal.addTextChangedListener(this);
                    return;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    withdrawal.addTextChangedListener(this);
                }

            }
        });


        if (ConnectivityReceiver.isConnected()) {
            BALANCE(true);
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }

    }

    private void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView title = (TextView) toolbar.findViewById(R.id.title);
        title.setText(getResources().getString(R.string.withdrawal));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void BALANCE(boolean is_show) {
        final ProgressDialog mDialog = util.initializeProgress(context);
        if (is_show)
            mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AUTH_KEY, "1234567");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(context, AllAPIS.BALANCE, formBody) {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonmainObject = new JSONObject(result);
                        if (jsonmainObject.getString("code").equalsIgnoreCase("200")) {
                            //  balance = Double.parseDouble(jsonmainObject.getJSONObject("data").getString("balance"));

                            /*
                            //  String s = String.format("%.2f", jsonmainObject.getJSONObject("data").getString("balance"));
                          //  BigDecimal bd1 = new BigDecimal(s.replace(",", "."));
                            //BigDecimal bd = new BigDecimal(bd1.stripTrailingZeros().toPlainString());

                            */

                            if (jsonmainObject.getJSONObject("data").getString("balance").contains(".")) {
                               // balance = Double.parseDouble(jsonmainObject.getJSONObject("data").getString("balance"));
                                //String s = truncateUptoTwoDecimal(Double.parseDouble(String.valueOf(balance)));
                                currentBalance.setText(savePref.getCurrency() + util.getDecimalFormattedString(jsonmainObject.getJSONObject("data").getString("balance")));
                            } else {
                               //long lll = Long.parseLong(jsonmainObject.getJSONObject("data").getString("balance"));
                                currentBalance.setText(savePref.getCurrency() + util.getDecimalFormattedString(jsonmainObject.getJSONObject("data").getString("balance")));
                            }





                           /* double dd1 = new BigDecimal(jsonmainObject.getJSONObject("data").getString("balance")).doubleValue();
                            String val1 = String.format("%.4f", dd1);
                            BigDecimal bd11 = BigDecimal.valueOf(Double.valueOf(val1));
                            Double d11 = bd11.doubleValue();

                            String s = String.format("%.2f", d11);
                            BigDecimal bd1 = new BigDecimal(s.replace(",", "."));
                            BigDecimal bd = new BigDecimal(bd1.stripTrailingZeros().toPlainString());

                            currentBalance.setText(savePref.getCurrency() + util.getDecimalFormattedString(String.valueOf(bd)));*/


                        } else {
                            if (jsonmainObject.getString("error_message").equals(util.Invalid_Authorization)) {
                                SavePref savePref = new SavePref(context);
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                context.overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.showToast(context, jsonmainObject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    public String truncateUptoTwoDecimal(double doubleValue) {
        String value = String.valueOf(doubleValue);
        if (value != null) {
            String result = value;
            int decimalIndex = result.indexOf(".");
            if (decimalIndex != -1) {
                String decimalString = result.substring(decimalIndex + 1);
                if (decimalString.length() > 2) {
                    result = value.substring(0, decimalIndex + 3);
                } else if (decimalString.length() == 1) {
                    result = String.format(Locale.ENGLISH, "%.2f",
                            Double.parseDouble(value));
                }
            }
            return result;
        }
        return null;
    }


    @OnClick(R.id.submit)
    public void onViewClicked() {
        if (ConnectivityReceiver.isConnected()) {
            if (withdrawal.getText().toString().isEmpty()) {
                util.IOSDialog(context, "Please Enter withdrawal Amount");
            } /*else if (Double.valueOf(withdrawal.getText().toString().replace(",", "")) > balance) {
                util.IOSDialog(context, "You Cannot Withdraw this amount.");
            }*/ else if (date.getText().toString().isEmpty()) {
                util.IOSDialog(context, "Please Select Date");
            } else if (description.getText().toString().isEmpty()) {
                util.IOSDialog(context, "Please Enter description");
            } else {
                WITHDRAWAL();
            }
        } else {
            util.IOSDialog(context, util.internet_Connection_Error);
        }
    }

    private void WITHDRAWAL() {
        util.hideKeyboard(context);
        final ProgressDialog mDialog = util.initializeProgress(context);
        mDialog.show();
        MultipartBody.Builder formBuilder = new MultipartBody.Builder();
        formBuilder.setType(MultipartBody.FORM);
        formBuilder.addFormDataPart(Parameters.AMOUNT, withdrawal.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.DESCRIPTION, description.getText().toString().replace(",", "").trim());
        formBuilder.addFormDataPart(Parameters.DATE, date_to_timestamp(date.getText().toString().replace(",", "").trim()));
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsync mAsync = new GetAsync(context, AllAPIS.WITHDRAWAL, formBody, savePref.getAuthorization_key()) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {
                            util.hideKeyboard(context);
                            util.IOSDialog(context, "WITHDRAWAL Sucessfully!!!");
                            withdrawal.setText("");
                            date.setText("");
                            description.setText("");
                            withdrawal.requestFocus();

                            BALANCE(false);

                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {
                                 savePref.setAuthorization_key("");
                                Intent intent = new Intent(context, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.IOSDialog(context, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }

    private String date_to_timestamp(String str_date) {

        // String ="13-09-2011";
        DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = (Date) formatter.parse(str_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println("Today is " + date.getTime());

        return String.valueOf(date.getTime() / 1000);

    }


}
