package com.heaven_bank_book.Adapters;

import android.content.Context;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.chauthai.swipereveallayout.SwipeRevealLayout;
import com.heaven_bank_book.Activities.HistoryActivity;
import com.heaven_bank_book.Activities.SeedsActivity;
import com.heaven_bank_book.Activities.TitheActivity;
import com.heaven_bank_book.Activities.WithdrawalActivity;
import com.heaven_bank_book.Models.TransectionModel;
import com.heaven_bank_book.R;
import com.heaven_bank_book.Util.SavePref;
import com.heaven_bank_book.Util.util;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.RecyclerViewHolder> {
    HistoryActivity context;
    LayoutInflater Inflater;
    ArrayList<TransectionModel> list;
    private View view;

    private SavePref savePref;

    public HistoryAdapter(HistoryActivity context, ArrayList<TransectionModel> list) {
        this.context = context;
        this.list = list;
        Inflater = LayoutInflater.from(context);

        savePref = new SavePref(context);

    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = Inflater.inflate(R.layout.history_row, parent, false);
        RecyclerViewHolder viewHolder = new RecyclerViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, final int position) {

        String currency = savePref.getCurrency();

        if (list.get(position).getType().equals("3")) {
            holder.amount.setText(currency + util.getDecimalFormattedString(list.get(position).getAmount()));
            holder.name.setText(list.get(position).getDescription());
            holder.amount.setTextColor(context.getResources().getColor(R.color.red));
            if (!list.get(position).getDate().isEmpty())
                holder.date.setText(util.convertTimeStampDateTime(Long.parseLong(list.get(position).getDate())));
        } else {
            if (!list.get(position).getDate().isEmpty())
                holder.date.setText(util.convertTimeStampDateTime(Long.parseLong(list.get(position).getDate())));
            holder.amount.setText(currency + util.getDecimalFormattedString(list.get(position).getAmount()));
            holder.amount.setTextColor(context.getResources().getColor(R.color.green));
            holder.name.setText(list.get(position).getChurch_name());

            if (list.get(position).getPetitions().isEmpty()) {
                holder.petitions.setVisibility(View.GONE);
            } else {
                //holder.petitions.setText(list.get(position).getPetitions());
                holder.petitions.setVisibility(View.GONE);
            }
            /*if (list.get(position).getType().equals("1")) {
                holder.type.setText("Seeds");
            } else if (list.get(position).getType().equals("2")) {
                holder.type.setText("Tithe");
            }*/
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.get(position).getType().equals("1") || list.get(position).getType().equals("2") || list.get(position).getType().equals("4")) {
                    context.next_task(list.get(position).getType(), position);
                }

            }
        });


        if (list.get(position).getType().equals("1") || list.get(position).getType().equals("2") || list.get(position).getType().equals("4")) {
            holder.type.setText(list.get(position).getDescription());
        } else {
            holder.type.setText("Withdrawal");
        }


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.petitions)
        TextView petitions;
        @BindView(R.id.type)
        TextView type;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.amount)
        TextView amount;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
