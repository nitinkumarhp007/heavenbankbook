package com.heaven_bank_book;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.aemerse.iap.DataWrappers;
import com.aemerse.iap.IapConnector;
import com.aemerse.iap.SubscriptionServiceListener;
import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchaseHistoryResponseListener;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.heaven_bank_book.Activities.ALMSActivity;
import com.heaven_bank_book.Activities.AppPurchaseActivity;
import com.heaven_bank_book.Activities.HistoryActivity;
import com.heaven_bank_book.Activities.ProfileActivity;
import com.heaven_bank_book.Activities.QuickViewActivity;
import com.heaven_bank_book.Activities.SeedsActivity;
import com.heaven_bank_book.Activities.SignInActivity;
import com.heaven_bank_book.Activities.TitheActivity;
import com.heaven_bank_book.Activities.WithdrawalActivity;
import com.heaven_bank_book.Util.Parameters;
import com.heaven_bank_book.Util.SavePref;
import com.heaven_bank_book.Util.VersionChecker;
import com.heaven_bank_book.Util.util;
import com.heaven_bank_book.parser.AllAPIS;
import com.heaven_bank_book.parser.GetAsync;
import com.heaven_bank_book.parser.GetAsyncGet;
import com.ligl.android.widget.iosdialog.IOSDialog;
import com.mf.library.OnCallBack;
import com.mf.library.UpdateChecker;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.FormBody;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();

    @BindView(R.id.profile_button)
    ImageView profileButton;
    @BindView(R.id.tithe)
    ImageView tithe;
    @BindView(R.id.seeds)
    ImageView seeds;
    @BindView(R.id.withdrawal)
    ImageView withdrawal;
    @BindView(R.id.history)
    ImageView history;
    @BindView(R.id.quick_view)
    ImageView quickView;
    @BindView(R.id.profile_pic)
    ImageView profilePic;
    @BindView(R.id.alms)
    ImageView alms;

    private SavePref savePref;
    MainActivity activity;
    Context context;
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInOptions googleSignInOptions;
    private static final int RC_SIGN_IN = 0;

    String fb_occupation = "", fb_username = "", fb_phone = "", fb_email = "", fb_image = "", social_id = "",
            fb_gender = "", fb_dob = "", fb_firstname = "", fb_lastname = "", socail_id = "";

    BillingClient mBillingClient;
    private String subscriptionMonthly = "monthly_1_20";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        activity = MainActivity.this;
        savePref = new SavePref(activity);

        Log.e("auth_key", new SavePref(this).getAuthorization_key());
        Log.e("auth_key", new SavePref(this).getID());

        googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        //Initializing google api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, googleSignInOptions)
                .build();

/*//        signInWithGplus();
        createBillingClientAndSetup();
        listOfProducts();*/


        Glide.with(this).load(savePref.getImage()).error(R.drawable.place_holder).into(profilePic);


    }

    @Override
    protected void onResume() {
        super.onResume();

        VersionChecker versionChecker = new VersionChecker();
        try {
            String app_version_store = versionChecker.execute().get();
            if (app_version_store != null) {
                if (!app_version_store.isEmpty()) {
                    String app_version_current = BuildConfig.VERSION_NAME;
                    //String app_version_current ="122";
                    Log.e("app_version___", app_version_store + "-" + app_version_current);
                    if (!app_version_store.equals(app_version_current)) {
                        UpdateApp();
                    } else {
                        setUpBilling();
                        //setSubscription_work();
                    }

                } else {
                    setUpBilling();

                }
            } else {
                setUpBilling();
            }


        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void setUpBilling() {
        savePref.setBoolean(util.SET_SUBSCRIBED, false);

        ArrayList<String> consumableKeys = new ArrayList<>();
        ArrayList<String> subscriptionKeys = new ArrayList<>();
        subscriptionKeys.add(util.MONTHLY_SUBSCRIPTION);

        IapConnector iapConnector = new IapConnector(context = this
                , consumableKeys, // pass the list of non-consumables
                consumableKeys, // pass the list of consumables
                subscriptionKeys, // pass the list of subscriptions
                util.LICENSE_KEY, true // pass your app's license keytrue // to enable / disable logging
        );

        iapConnector.addSubscriptionListener(new SubscriptionServiceListener() {
            @Override
            public void onSubscriptionRestored(@NonNull DataWrappers.PurchaseInfo purchaseInfo) {
                Log.e("Purchase", "is " + purchaseInfo.getSku());
                if (purchaseInfo.getSku().equals(util.MONTHLY_SUBSCRIPTION)) {
                    savePref.setBoolean(util.SET_SUBSCRIBED, true);
                }
                boolean is_subscription_on = savePref.getBoolean(util.SET_SUBSCRIBED, false);
                if (!is_subscription_on) {
                    setSubscription_work();
                }


            }

            @Override
            public void onSubscriptionPurchased(@NonNull DataWrappers.PurchaseInfo purchaseInfo) {

            }

            @Override
            public void onPricesUpdated(@NonNull Map<String, DataWrappers.SkuDetails> map) {

            }
        });


    }


    private void setSubscription_work() {
        if (!savePref.getBoolean(util.SET_SUBSCRIBED, false)) {
            new IOSDialog.Builder(this)
                    .setCancelable(false)
                    .setTitle(this.getResources().getString(R.string.app_name))
                    .setMessage(getString(R.string.monthly_subscription)).setPositiveButton(getString(R.string.proceed), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int which) {
                    dialogInterface.dismiss();
                    Intent intent = new Intent(activity, AppPurchaseActivity.class);
                    //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                }
            })
                    .setNegativeButton(getString(R.string.exit), (dialog, which) -> {
                        savePref.setAuthorization_key("");
                        Intent intent = new Intent(this, SignInActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                    }).show();

        }
    }

    private void UpdateApp() {
        new IOSDialog.Builder(this)
                .setCancelable(false)
                .setTitle(this.getResources().getString(R.string.app_name))
                .setMessage(getString(R.string.google_play_store)).setPositiveButton(getString(R.string.update), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                dialogInterface.dismiss();
                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object

                Intent intent = null;
                try {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
                } catch (ActivityNotFoundException anfe) {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        })
                .setNegativeButton(getString(R.string.exit), (dialog, which) -> {
                    this.finishAffinity();
                }).show();
    }

    /*private void createBillingClientAndSetup() {
        mBillingClient = BillingClient
                .newBuilder(activity)
                .setListener(this)
                .build();

        mBillingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(int responseCode) {
                if (responseCode == BillingClient.BillingResponse.OK) {
//                    Toast.makeText(activity, "Successfully connected to the billing client", Toast.LENGTH_SHORT).show();
                    checkPurchaseHistory();


                } else {
                    Toast.makeText(activity, "Failed to connect to the billing client", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onBillingServiceDisconnected() {
                Toast.makeText(activity, "Disconnected to the billing client", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private void checkPurchaseHistory() {

        mBillingClient.queryPurchaseHistoryAsync(BillingClient.SkuType.SUBS, new PurchaseHistoryResponseListener() {
            @Override
            public void onPurchaseHistoryResponse(int responseCode, List<Purchase> purchasesList) {
                if (responseCode == BillingClient.BillingResponse.OK && purchasesList.size() != 0) {
                    // Toast.makeText(activity, "Disconnected to the billing client", Toast.LENGTH_SHORT).show();

                } else {
                    goToPurchaseScreen();
                }
            }
        });
    }


    private void listOfProducts() {
        List<String> skuList = new ArrayList<>();
        skuList.add(subscriptionMonthly);

        SkuDetailsParams.Builder params = SkuDetailsParams.newBuilder();
        params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
        mBillingClient.querySkuDetailsAsync(params.build(), new SkuDetailsResponseListener() {
            @Override
            public void onSkuDetailsResponse(int responseCode, List<SkuDetails> skuDetailsList) {
                if (skuDetailsList != null && responseCode == BillingClient.BillingResponse.OK) {
                    if (skuDetailsList.size() == 0) {
                        goToPurchaseScreen();
                    } else {
                        for (SkuDetails skuDetails : skuDetailsList) {
                            String sku = skuDetails.getSku();
                            String price = skuDetails.getPrice();
                            if (subscriptionMonthly.equals(sku)) {
//                            Toast.makeText(activity, subscriptionMonthly, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                } else {
                    goToPurchaseScreen();
                }
            }
        });
    }
*/

    private void goToPurchaseScreen() {
        if (savePref.getStringLatest("is_premium").equals("1")) {

        }

    }

    private void signInWithGplus() {

        //Creating an intent
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        //Starting intent for result
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //for google+ login
        if (resultCode == RESULT_OK) {
            if (requestCode == RC_SIGN_IN) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                //Calling a new function to handle signin
                handleSignInResult(result);
            }
        }
    }

    //After the signing we are calling this function
    private void handleSignInResult(GoogleSignInResult result) {

        //If the login succeed
        if (result.isSuccess()) {
            //Getting google account
            GoogleSignInAccount acct = result.getSignInAccount();
            social_id = acct.getId();
            fb_username = acct.getGivenName();
            fb_email = acct.getEmail();
            Uri personPhoto = acct.getPhotoUrl();
            String username = acct.getDisplayName();
            IN_APP_API(fb_email);

        } else {
            //If login fails
            Toast.makeText(this, "" + result.getStatus().toString(), Toast.LENGTH_LONG).show();
        }

    }

    private void IN_APP_API(String gmail) {
        final ProgressDialog mDialog = util.initializeProgress(activity);
        mDialog.show();
        FormBody.Builder formBuilder = new FormBody.Builder();
        formBuilder.add(Parameters.GMAIL_ACCOUNT, "");
        RequestBody formBody = formBuilder.build();
        @SuppressLint("StaticFieldLeak") GetAsyncGet mAsync = new GetAsyncGet(activity, AllAPIS.IN_APP + "?gmail_account=" + gmail, formBody) {
            @Override
            public void getValueParse(String result) {
                mDialog.dismiss();
                if (result != null && !result.equalsIgnoreCase("")) {
                    try {
                        JSONObject jsonMainobject = new JSONObject(result);
                        if (jsonMainobject.getString("code").equalsIgnoreCase("200")) {

                            if (jsonMainobject.getJSONObject("data").length() == 0) {
                                Intent intent = new Intent(activity, AppPurchaseActivity.class);
                                intent.putExtra("auth_key", savePref.getAuthorization_key());
                                intent.putExtra("gmail", gmail);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                int date = Integer.parseInt(jsonMainobject.getJSONObject("data").getString("date"));

                                Long tsLong = System.currentTimeMillis() / 1000;
                                String ts1 = tsLong.toString();
                                int ts = Integer.parseInt(ts1);

                                if (date < ts) {
                                    //in app
                                    Intent intent = new Intent(activity, AppPurchaseActivity.class);
                                    intent.putExtra("auth_key", savePref.getAuthorization_key());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                                }

                            }

                        } else {
                            if (jsonMainobject.getString("error_message").equals(util.Invalid_Authorization)) {

                                savePref.setAuthorization_key("");
                                Intent intent = new Intent(activity, SignInActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                            } else {
                                util.IOSDialog(activity, jsonMainobject.getString("error_message"));
                            }
                        }
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }

            @Override
            public void retry() {

            }
        };
        mAsync.execute();
    }


    @OnClick({R.id.profile_pic, R.id.tithe, R.id.seeds, R.id.withdrawal, R.id.history, R.id.quick_view, R.id.alms})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.profile_pic:
                startActivity(new Intent(this, ProfileActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.tithe:
                startActivity(new Intent(this, TitheActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.seeds:
                startActivity(new Intent(this, SeedsActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.withdrawal:
                startActivity(new Intent(this, WithdrawalActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.history:
                startActivity(new Intent(this, HistoryActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.quick_view:
                startActivity(new Intent(this, QuickViewActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
            case R.id.alms:
                startActivity(new Intent(this, ALMSActivity.class));
                overridePendingTransition(R.anim.zoom_enter, R.anim.zoom_exit);
                break;
        }
    }
}
