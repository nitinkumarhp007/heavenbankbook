package com.heaven_bank_book.Models;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

import java.util.ArrayList;

public class TransectionModel implements Parcelable {

    public TransectionModel()
    {

    }
    String id="";
    String type="";
    String amount="";
    String description="";
    String total="";
    ArrayList<TransectionModel>list=new ArrayList<>();
    String created="";
    String church_name="";
    String date="";
    String petitions="";
    String return_expection="";
    String earth_measure="";
    String heaven_measure="";
    String balance_based_per="";
    String turn_over="";
    String sale_cost="";
    String gross_income="";
    String balance="";


    protected TransectionModel(Parcel in) {
        id = in.readString();
        type = in.readString();
        amount = in.readString();
        description = in.readString();
        total = in.readString();
        list = in.createTypedArrayList(TransectionModel.CREATOR);
        created = in.readString();
        church_name = in.readString();
        date = in.readString();
        petitions = in.readString();
        return_expection = in.readString();
        earth_measure = in.readString();
        heaven_measure = in.readString();
        balance_based_per = in.readString();
        turn_over = in.readString();
        sale_cost = in.readString();
        gross_income = in.readString();
        balance = in.readString();
    }

    public static final Creator<TransectionModel> CREATOR = new Creator<TransectionModel>() {
        @Override
        public TransectionModel createFromParcel(Parcel in) {
            return new TransectionModel(in);
        }

        @Override
        public TransectionModel[] newArray(int size) {
            return new TransectionModel[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getReturn_expection() {
        return return_expection;
    }

    public void setReturn_expection(String return_expection) {
        this.return_expection = return_expection;
    }

    public String getEarth_measure() {
        return earth_measure;
    }

    public void setEarth_measure(String earth_measure) {
        this.earth_measure = earth_measure;
    }

    public String getHeaven_measure() {
        return heaven_measure;
    }

    public void setHeaven_measure(String heaven_measure) {
        this.heaven_measure = heaven_measure;
    }

    public String getBalance_based_per() {
        return balance_based_per;
    }

    public void setBalance_based_per(String balance_based_per) {
        this.balance_based_per = balance_based_per;
    }

    public String getTurn_over() {
        return turn_over;
    }

    public void setTurn_over(String turn_over) {
        this.turn_over = turn_over;
    }

    public String getSale_cost() {
        return sale_cost;
    }

    public void setSale_cost(String sale_cost) {
        this.sale_cost = sale_cost;
    }

    public String getGross_income() {
        return gross_income;
    }

    public void setGross_income(String gross_income) {
        this.gross_income = gross_income;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public ArrayList<TransectionModel> getList() {
        return list;
    }

    public void setList(ArrayList<TransectionModel> list) {
        this.list = list;
    }

    public String getChurch_name() {
        return church_name;
    }

    public void setChurch_name(String church_name) {
        this.church_name = church_name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAmount() {
        return amount;
    }

    public String getPetitions() {
        return petitions;
    }

    public void setPetitions(String petitions) {
        this.petitions = petitions;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(type);
        dest.writeString(amount);
        dest.writeString(description);
        dest.writeString(total);
        dest.writeTypedList(list);
        dest.writeString(created);
        dest.writeString(church_name);
        dest.writeString(date);
        dest.writeString(petitions);
        dest.writeString(return_expection);
        dest.writeString(earth_measure);
        dest.writeString(heaven_measure);
        dest.writeString(balance_based_per);
        dest.writeString(turn_over);
        dest.writeString(sale_cost);
        dest.writeString(gross_income);
        dest.writeString(balance);
        dest.writeString(petitions);
    }
}

