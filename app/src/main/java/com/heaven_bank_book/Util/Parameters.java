package com.heaven_bank_book.Util;

public class Parameters {
    public static final String AUTHORIZATION_KEY = "authorization_key";
    public static final String GMAIL_ACCOUNT = "gmail_account";
    public static final String AUTH_KEY = "auth_key";
    public static final String DEVICE_TOKEN = "device_token";
    public static final String EMAIL = "email";
    public static final String SOCIAL_ID = "social_id";
    public static final String SOCIAL_TYPE = "soical_type";
    public static final String OTP = "otp";
    public static final String DEVICE_TYPE = "device_type";
    public static final String NAME = "name";
    public static final String PASSWORD = "password";
    public static final String PHONE = "phone";
    public static final String COUNTRY_CODE = "country_code";
    public static final String COUNTRY = "country";
    public static final String OLD_PASSWORD = "old_password";
    public static final String NEW_PASSWORD = "new_password";
    public static final String PROFILE = "profile";

    public static final String CHURCH_NAME = "church_name";
    public static final String RETURN_EXPECTION = "return_expection";
    public static final String EARTH_MEASURE = "earth_measure";
    public static final String HEAVEN_MEASURE = "heaven_measure";
    public static final String BALANCE_BASED_PER = "balance_based_per";
    public static final String AMOUNT = "amount";
    public static final String DESCRIPTION = "description";
    public static final String PETITIONS = "petitions";

    public static final String TURN_OVER = "turn_over";
    public static final String SALE_COST = "sale_cost";
    public static final String GROSS_INCOME = "gross_income";
    public static final String TIME = "time";
    public static final String BALANCE = "balance";
    public static final String DATE = "date";


    public static final String TRANSACTION_ID = "transaction_id";
    public static final String TITHE_ID = "tithe_id";
    public static final String SEED_ID ="seed_id" ;
    public static final String ALMS_ID ="alms_id" ;
    public static final String TYPE ="type" ;
    public static final String TO_DATE ="to_date" ;
    public static final String FROM_DATE ="from_date" ;
}















