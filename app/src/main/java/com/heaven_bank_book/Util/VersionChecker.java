package com.heaven_bank_book.Util;

import android.os.AsyncTask;
import android.util.Log;

import com.heaven_bank_book.MainActivity;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class VersionChecker extends AsyncTask<String, String, String> {

    String newVersion=null;

    @Override
    protected String doInBackground(String... params) {
        try {
            Document document = Jsoup.connect("http://play.google.com/store/apps/details?id=com.heaven_bank_book&hl=en")
                    .timeout(30000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get();
            newVersion = document.select("div[itemprop=softwareVersion]").first().ownText();

                Log.e("newVersion", newVersion);

                return newVersion;


            } catch (Exception e) {
                return newVersion;
            }
        }


}