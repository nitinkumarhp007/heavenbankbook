package com.heaven_bank_book.parser;


public class AllAPIS {

    //public static final String BASE_URL = "https://theheavenbankbook.com/apis/v1/";
    public static final String BASE_URL = "http://18.220.10.58:4002/apis/v1/";


    public static final String USERLOGIN = BASE_URL + "user/login";
    public static final String USER_SIGNUP = BASE_URL + "user";
    public static final String SOCIAL_LOGIN = BASE_URL + "soical_login";
    public static final String VERIFY_OTP = BASE_URL + "user/verifiy";
    public static final String FORGOT_PASSWORD = BASE_URL + "forgot_password";
    public static final String LOGOUT = BASE_URL + "user/logout";
    public static final String CHANGEPASSWORD = BASE_URL + "user/change_password";
    public static final String EDIT_PROFILE = BASE_URL + "user/edit";
    public static final String APP_INFO = BASE_URL + "app_info";

    public static final String ADD_SEED = BASE_URL + "add-seed";
    public static final String ADD_TITHE = BASE_URL + "add-tithe";

    public static final String ADD_ALMS = BASE_URL + "add-alms";
    public static final String WITHDRAWAL = BASE_URL + "withdrawal";
    public static final String TRANSACTIONS = BASE_URL + "transactions";
    public static final String BALANCE = BASE_URL + "balance";
    public static final String SEND_MAIL = BASE_URL + "send-mail";
    public static final String IN_APP = BASE_URL + "in_app";
    public static final String GET_BALANCE_FILTER = BASE_URL + "get-balance-filter";

    //terms link
    //https://www.privacypolicies.com/terms/view/2180bb53f2b85175caa9f13a9abd7cea

    //privacy policy link
    //https://www.freeprivacypolicy.com/privacy/view/7edaa1577ce61889fc201bea4b9a1a85
}
